# py_spring_hri

version 0.0.3
(07.04.2023)

The py_spring_hri package provides an interface to subscribe to ROS4HRI topics.

Features:
 * Subscription to tracked ROS4HRI entities: persons, bodies, faces, voices, and groups
 * Subscription to default and custom properties of ROS4HRI entities
 * Storage and access of received ros messages via dictionaries
 * Callback functions if new information about tracked entities is received
 * Easy access to transforms (TFs) of ROS4HRI entities
 
The central element of the py_spring_hri package is the _HRIListener_ class that allows to subscribe and access the ROS4HRI topics.  

The following example looks in a loop of 1Hz at all detected persons.

```python
import rospy
from py_spring_hri import HRIListener

rospy.init_node("pyhri_test")

# subscribe to all entities and their default properties
hri = HRIListener.start_with_default_properties()

# do something in a loop with 1Hz
rate = rospy.Rate(1)
while not rospy.is_shutdown():

    # access detected persons
    with hri.tracked_persons.lock:
        for person_id, person in hri.tracked_persons.items():
            print('Tracking person {} (name: {}):'.format(person_id, person.name))
          
            if person.body:
                print('\thas body {}'.format(person.body.id))

            if person.face:
                print('\thas face {}'.format(person.face.id))

            if person.voice:
                print('\thas voice {}'.format(person.voice.id))
            
    rate.sleep()  

hri.close()
```

This example prints a message in the case of a change in the list of tracked persons:

```python
import rospy
from py_spring_hri import HRIListener

def on_tracked_persons(msg):
    print('tracked person ids: {}'.format(msg.ids))

rospy.init_node("pyhri_test")

hri = HRIListener.with_default_properties()	
hri.add_tracked_person_callback(on_tracked_persons)
hri.start()

rospy.spin()  

hri.close()
```





## Defining which entities and properties are followed

There are several ways to define which entities and properties should be subscribed to.
The constructor of the _HRIListener_ allows to set all this information.
Properties that should be subscribed can also be added via the `add_<ENTITY>_property()` methods.

### Subscribing to all ROS4HRI entities and default/required properties

A special constructors exist to create a HRIListener that subscribes to all entities and their default properties.
It also starts the HRIListener directly:
```python
hri = HRIListener.start_with_default_properties()
```
If the HRIListener should not be started directly, for example, to add custom properties or callback functions use:
```python
hri = HRIListener.with_default_properties()
# add custom properties
# add callback functions
hri.start()
```

Similart constructors also exist to subscribe only to required properties: 
 * `HRIListener.start_with_required_properties()`, and
 * `HRIListener.with_required_properties()` 


### Subscribing only to certain entities with their default/required properties

If only specific entities should be followed, for example, persons and voices (no faces, bodies, or groups), then the default and required constructors allow to remove the other entities via:

```python
hri = HRIListener.start_with_default_properties(subscribe_faces=False, 
                                                subscribe_bodies=False, 
                                                subscribe_groups=False)
```


### Subscribing only to specific properties

If only certain properties should be followed, then the default HRIConstructor can be used.
One way is to define which properties should be used by defining them in its arguments.

For example, if only persons, voices, and faces should be followed with the properties person/name, person/native_language and voice/speech:
```python
hri = HRIListener.start(
    person_properties = ['name', 'native_language'],
    voice_properties = ['speech'],
    subscribe_faces = True)
```

It is also possible to add properties using the `add_<ENTITY>_property()` methods before the HRIListener is started:
```python
hri = HRIListener(subscribe_faces = True)
hri.add_person_property('name')
hri.add_person_property('native_language')
hri.add_voice_property('speech')
hri.start()
```

### Subscribing to custom properties

Each entity (person, body, face, voice, group) can have custom properties that are not in the ROS4HRI standard.
For example the height of a body could be published at '/humans/bodies/<body_id>/height'.
Such custom properties can be added by defining their name, the ROS message type, and optionally a data function that converts the ROS message into a custom data format. 

If the HRIListener constructor is used, custom properties are defined by a tuple with (name, ROS_Message_Type [, data_function]) where
 * name: String with the name of the property, for example, 'height'
 * ROS_Message_Type: Class of the ROS Message, for example, std_msgs.msg.Float32
 * data_function: An optional function that takes as input the ROS message and outputs the data that the property should have. The default data is is the message itself.

The following example subscribes all entities and default properties with 2 additional properties.
One has a special data function, the other not.
```python
import rospy
from py_spring_hri import HRIListener
from std_msgs.msg import Bool, Float32

hri = HRIListener.start_with_default_properties(
   body_properties=[
      # custom property for height which gets converted from m into cm
      ('height', Float32, lambda msg: msg.data * 100), 
      # custom property that defines if the body is sitting 
      ('is_sitting', Bool)
   ]
)

# access custom properties
rate = rospy.Rate(1)
while not rospy.is_shutdown():
   with hri.bodies.lock:
       for body in hri.bodies.values():
           print('Body {} has a height of {}cm.'.format(body.id, body.height))
         
           if body.is_sitting.data:
               print('\t It is sitting.')
   
           rate.sleep()
```

Custom properties can also be added using the `add_<ENTITY>_property()` methods before the HRIListener is started:
```python
hri = HRIListener()
hri.add_body_property('height', Float32, lambda msg: msg.data * 100)
hri.add_body_property('is_sitting', Bool)
hri.start()
```

Note: Default properties from ROS4HR can be changed by redefining them as custom properties.


### Data function for images

A special data function exists for images that converts them from the ROS message via OpenCV into a readable format.
This is used for the following properties:
 * face/cropped
 * face/aligned
 * face/frontalized
 * body/cropped

If this should be used for custom properties use the string 'imgmsg_to_cv2' for the data function.

TODO: example code


## Accessing the data

The tracked entities and their properties can be accessed via dictionaries.

For each entity exists a dictionary (_bodies_, _faces_, _voices_, _groups_).
Persons have two dictionaries, one for the tracked persons (_tracked_persons_) and one for known persons (_known_persons_).
The dictionaries contain objects that represent the currently tracked entities which are published under the respective '/human/{Entity}/tracked' topic.
The objects in the dictionaries represent the different entities, such as bodies.
They can be used to access their properties.
Dictionaries also have a _lock_ which has to be activated while using them.

The following example prints the tracked voices and some of their properties.

```python
hri = HRIListener.start_with_default_properties()

rate = rospy.Rate(1)
while not rospy.is_shutdown():

    # need to lock the voices dictionary, otherwise there could be exceptions
    with hri.voices.lock:
        
       # read header of last /humans/voices/tracked IdsList message
        print('information from {}:'.format(hri.voices.header.sec))
      
        for voice_id, voice in hri.voices:
            print('tracked voice {}"'.format(voice_id))
            print('\t is_speaking: {}'.format(voice.is_speaking))
            print('\t speech - incremental: {}'.format(voice.speech.incremental))
            print('\t speech - final: {}'.format(voice.speech.final))

        rate.sleep()
```

### Reading the header 

The dictionaries have an additional _header_ property that holds the header from the last received IDsList message of the respective tracked topic.
The header is a [std_msgs.msg.Header](https://docs.ros.org/en/noetic/api/std_msgs/html/msg/Header.html).

```python
header = hri.voices.header
```

### Copy of dictionaries

It is also possible to get a copy of dictionaries, which do not need to be locked.
3 different copies are possible:
 1) _Regular Copy_: Will not have entities added or removed but the existing entities receive updates for their properties.
 2) _Data Copy_: Copy where the properties of entities will not receive further updates. 
 3) _Data Deep Copy_: Data copy but with a deep copy of all data. 

Otherwise, errors might occur while iterating through a dictionary while its respective tracked topic is updated and elements in the dictionary might be deleted or added.
It is also possible to create copies of the dictionaries which avoid this problem.

```python
hri = HRIListener.start_with_default_properties()

rate = rospy.Rate(1)
while not rospy.is_shutdown():

    # get copy of the voices dictionary which must not be locked anymore
    voices = hri.copy_voices()
    
    # use this for a data copy
    #voices = hri.copy_voices_data()
    
    # use this for a deep data copy
    #voices = hri.deepcopy_voices_data()
   
    # read header of last /humans/voices/tracked IdsList message
    print('information from {}:'.format(voices.header.sec))
   
    for voice_id, voice in voices:
        print('tracked voice {}"'.format(voice_id))
        print('\t is_speaking: {}'.format(voice.is_speaking))
        print('\t speech - incremental: {}'.format(voice.speech.incremental))
        print('\t speech - final: {}'.format(voice.speech.final))
        
    rate.sleep()
```

### Existing properties

This is a list of all existing default (and required) properties per entity and their data type.

Body:
 * roi (required): sensor_msgs.msg.RegionOfInterest
 * cropped (required): py_spring_hri.misc.Cv2Image
 * skeleton2d: hri_msgs.msg.Skeleton2D
 * posture: hri_msgs.msg.BodyPosture
 * gesture: hri_msgs.msg.Gesture

Face:
 * roi (required): sensor_msgs.msg.RegionOfInterest
 * cropped (required): py_spring_hri.misc.Cv2Image
 * aligned: py_spring_hri.misc.Cv2Image
 * frontalized: py_spring_hri.misc.Cv2Image
 * landmarks: hri_msgs.msg.FacialLandmarks
 * facs: hri_msgs.msg.FacialActionUnits
 * expression: hri_msgs.msg.Expression
 * softbiometrics: hri_msgs.msg.SoftBiometrics
 

Voice:
 * audio (required): audio_common_msgs.msg.AudioData
 * features (required): hri_msgs.msg.AudioFeatures
 * is_speaking: bool
 * speech: hri_msgs.msg.LiveSpeech

Person:
 * anonymous (required): bool
 * face_id (will always be subscribed): string
   * face: Face entity
 * body_id (will always be subscribed): string
   * body: Body entity
 * voice_id (will always be subscribed): string
   * voice: Voice entity
 * alias: string
 * engagement_status: hri_msgs.msg.EngagementLevel
 * location_confidence: float
 * name: string
 * native_language: string

Group:
 * member_ids (will always be subscribed): list of strings
   * members: Dictionary with persons that are members of the group

   
## Callback functions

It is possible to register callback functions in the case that ROS messages for tracked persons, faces, bodies, voices, groups or their properties are received.

### Callback functions for tracked entities

Use the `add_<Entity>_callback` methods to register callbacks for tracked enities. 

```python
def on_tracked_persons(msg):
    print('tracked person ids: {}'.format(msg.ids))

hri = HRIListener.with_default_properties()	
hri.add_tracked_person_callback(on_tracked_persons)
hri.start()

rospy.spin()
```
 
### Callback functions for tracked properties

Use the `add_<Entity>_callback` methods to register callbacks for tracked enities. 

```python
def on_voice_speech(entity, data, msg):
    # receives hri_msgs.msg.LiveSpeech messages from the voice/speech property 
    print('Voice {} said something: {} ...'.format(entity.id, msg.incremental))

hri = HRIListener.with_default_properties()	
hri.add_voice_property_callback('speech', on_voice_speech)
hri.start()

rospy.spin()
```

## TFs

Entities can have TFs which define their position and orientation.
For example the position of a body with ID '3' will be published as TF `/body_3`.
By default TFs shoudl be published under the following names:
 * persons: `/person_<person_id>`
 * body: `/body_<body_id>`
 * face: `/face_<face_id>`
   * gaze: `/gaze_<gaze_id>`
   * focus: `/focus_<focus_id>`
 * group: `/group_<group_id>`
 
This example accesses the person and all face TFs:
```python
hri = HRIListener.start_with_default_properties()	

for person in hri.persons.values():
    p_tf = person.transform()

    face_tf = person.face.transform()
    gaze_tf = person.face.transform_gaze()
    focus_tf = person.face.transform_focus(target_frame='map')
```

### Definition of the target frame

The reference frame for the TFs can be either defined when creating the HRIListener setting the _default_target_frame_ argument 
or by setting the _target_frame_ argument of the `transform()` methods (See example above).
The preset default is 'base_link'.

Setting a different default target frame:
```python
hri = HRIListener.start_with_default_properties(default_target_frame='map')	
```


## Change of default namespaces

The py_spring_hri package allows to change several default values including the namespaces of topics and TFs.

### Entity namespaces

ROS4HRI uses as default namespace for its entities:
 * /humans/persons
 * /humans/bodies
 * /humans/faces
 * /humans/voices
 * /humans/groups

To change them use:
```python
import py_spring_hri as pyhri
from py_spring_hri import HRIListener

pyhri.default.person_namespace = '/hri/persons'
pyhri.default.body_namespace = '/hri/bodies'
pyhri.default.face_namespace = '/hri/faces'
pyhri.default.voice_namespace = '/hri/voices'
pyhri.default.group_namespace = '/hri/groups'

hri = HRIListener.start_with_default_properties()	
```

### TF namespaces

Also TF namespaces can be changed. 
They are stored as templates, for example, as 'person_{}' where '{}' will be replaced by the person ID (using the string format method).
 * body: 'body_{}'
 * face: 'face_{}'
 * gaze: 'gaze_{}'
 * focus: 'focus_{}'
 * voice: 'voice_{}'
 * person: 'person_{}'
 * group: 'group_{}'

Example where the TF for groups is changed from 'group_{groupID}' to 'grp_{groupID}':
```python
import py_spring_hri as pyhri
from py_spring_hri import HRIListener

pyhri.default.group_tf_template = 'grp_{}'

hri = HRIListener.start_with_default_properties()	
```
