import multiprocessing
import rospy
from hri_msgs.msg import IdsList
from cv_bridge import CvBridge
import copy
import tf2_ros
from .person import Person
from .body import Body
from .face import Face
from .voice import Voice
from .group import Group
from .misc import PropertyDescription, EntityDict
from .defaults import defaults

# TODO: allow to set callbacks in constructor
# TODO: add person field to face, body, voice

class HRIListener:

    @property
    def started(self):
        return self._started


    @property
    def subscribe_persons(self):
        return self._subscribe_persons

    @subscribe_persons.setter
    def set_subscribe_persons(self, subscribe_persons):
        if self.started:
            raise ValueError('Can not subscribe to additional entities (bodies, face, voices, persons, groups) after the subscriber is started!')
        self._subscribe_persons = subscribe_persons


    @property
    def subscribe_bodies(self):
        return self._subscribe_bodies

    @subscribe_bodies.setter
    def set_subscribe_bodies(self, subscribe_bodies):
        if self.started:
            raise ValueError('Can not subscribe to additional entities (bodies, face, voices, persons, groups) after the subscriber is started!')
        self._subscribe_bodies = subscribe_bodies


    @property
    def subscribe_faces(self):
        return self._subscribe_faces

    @subscribe_faces.setter
    def set_subscribe_faces(self, subscribe_faces):
        if self.started:
            raise ValueError('Can not subscribe to additional entities (bodies, face, voices, persons, groups) after the subscriber is started!')
        self._subscribe_faces = subscribe_faces


    @property
    def subscribe_voices(self):
        return self._subscribe_voices

    @subscribe_voices.setter
    def set_subscribe_voices(self, subscribe_vocies):
        if self.started:
            raise ValueError('Can not subscribe to additional entities (bodies, face, voices, persons, groups) after the subscriber is started!')
        self._subscribe_voices = subscribe_vocies


    @property
    def subscribe_groups(self):
        return self._subscribe_groups

    @subscribe_groups.setter
    def set_subscribe_groups(self, subscribe_groups):
        if self.started:
            raise ValueError('Can not subscribe to additional entities (bodies, face, voices, persons, groups) after the subscriber is started!')
        self._subscribe_groups = subscribe_groups


    @classmethod
    def start_with_default_properties(cls, sleep=0.5, **kwargs):
        hri = HRIListener.with_default_properties(**kwargs)
        hri.start(sleep=sleep)
        return hri

    @classmethod
    def with_default_properties(cls,
                                subscribe_persons=True, subscribe_bodies=True, subscribe_faces=True, subscribe_voices=True, subscribe_groups=True,
                                person_properties=None, body_properties=None, face_properties=None, voice_properties=None, group_properties=None,
                                **kwargs):

        if person_properties is None: person_properties = []
        if body_properties is None: body_properties = []
        if face_properties is None: face_properties = []
        if voice_properties is None: voice_properties = []
        if group_properties is None: group_properties = []

        def_person_properties = list(defaults.person_property_descriptions.values()) if subscribe_persons else []
        def_body_properties = list(defaults.body_property_descriptions.values()) if subscribe_bodies else []
        def_face_properties = list(defaults.face_property_descriptions.values()) if subscribe_faces else []
        def_voice_properties = list(defaults.voice_property_descriptions.values()) if subscribe_voices else []
        def_group_properties = list(defaults.group_property_descriptions.values()) if subscribe_groups else []

        return cls(
            subscribe_persons=subscribe_persons, subscribe_bodies=subscribe_bodies, subscribe_faces=subscribe_faces,
            subscribe_voices=subscribe_voices, subscribe_groups=subscribe_groups,
            person_properties=def_person_properties + person_properties,
            body_properties=def_body_properties + body_properties,
            face_properties=def_face_properties + face_properties,
            voice_properties=def_voice_properties + voice_properties,
            group_properties=def_group_properties + group_properties,
            **kwargs
        )


    @classmethod
    def start_with_required_properties(cls, sleep=0.5, **kwargs):
        hri = HRIListener.with_required_properties(**kwargs)
        hri.start(sleep=sleep)
        return hri

    @classmethod
    def with_required_properties(cls,
                                subscribe_persons=True, subscribe_bodies=True, subscribe_faces=True, subscribe_voices=True, subscribe_groups=True,
                                person_properties=None, body_properties=None, face_properties=None, voice_properties=None, group_properties=None,
                                **kwargs):

        if person_properties is None: person_properties = []
        if body_properties is None: body_properties = []
        if face_properties is None: face_properties = []
        if voice_properties is None: voice_properties = []
        if group_properties is None: group_properties = []

        req_person_properties = [defaults.person_property_descriptions['anonymous']] if subscribe_persons else []
        req_body_properties = [defaults.body_property_descriptions['roi'], defaults.body_property_descriptions['cropped']] if subscribe_bodies else []
        req_face_properties = [defaults.face_property_descriptions['roi'], defaults.face_property_descriptions['cropped']] if subscribe_faces else []
        req_voice_properties = [defaults.voice_property_descriptions['audio']] if subscribe_voices else []

        return cls(
            subscribe_persons=subscribe_persons, subscribe_bodies=subscribe_bodies, subscribe_faces=subscribe_faces,
            subscribe_voices=subscribe_voices, subscribe_groups=subscribe_groups,
            person_properties=req_person_properties + person_properties,
            body_properties=req_body_properties + body_properties,
            face_properties=req_face_properties + face_properties,
            voice_properties=req_voice_properties + voice_properties,
            group_properties=group_properties,
            **kwargs
        )


    @classmethod
    def start(cls, sleep=0.5, **kwargs):
        hri = cls(**kwargs)
        hri.start(sleep=sleep)
        return hri


    @staticmethod
    def _get_property_descriptions_from_input(default_property_descriptions, input_arg):

        property_descriptions = []
        if input_arg:
            for input_descr in input_arg:
                if isinstance(input_descr, str):
                    if input_descr in default_property_descriptions:
                        property_descriptions.append(default_property_descriptions[input_descr])
                    else:
                        raise ValueError('Default property {} does not exist!'.format(input_descr))

                elif isinstance(input_descr, PropertyDescription):
                    property_descriptions.append(input_descr)

                elif isinstance(input_descr, tuple):

                    if len(input_descr) != 2 and len(input_descr) != 3:
                        raise ValueError('Tuples that describe properties must be of length 2 or 3: (name, ROSMessage [,data_function])!')

                    name = input_descr[0]
                    ros_msg = input_descr[1]

                    if len(input_descr) > 2:
                        data_function = input_descr[2]
                    else:
                        data_function = None

                    property_descriptions.append(PropertyDescription(name, ros_msg, data_function))

                else:
                    ValueError('Invalid property description format. Must be of type string, py_spring_hri.PropertyDescription or a tuple with (name, ROSMessage [,data_function])!')

        return property_descriptions

    def __init__(self, subscribe_persons=False, subscribe_bodies=False, subscribe_faces=False, subscribe_voices=False, subscribe_groups=False,
                 person_properties=None, body_properties=None, face_properties=None, voice_properties=None, group_properties=None,
                 default_target_frame='base_link'):

        # redefine the start method of the object, so that we can have a classmethod and an object method with the same name
        # see https://stackoverflow.com/questions/20533349/python-regular-method-and-static-method-with-same-name
        self.start = self._start

        self._started = False
        self.default_target_frame = default_target_frame
        self._tf_buffer = tf2_ros.Buffer()
        self._tf_listener = tf2_ros.TransformListener(self._tf_buffer)
        self._cv_bridge = CvBridge()

        # PERSONS
        self._subscribe_persons = subscribe_persons
        self._person_property_descriptions = self._get_property_descriptions_from_input(defaults.person_property_descriptions, person_properties)
        self._person_property_callbacks = []
        if self._person_property_descriptions:
            self._subscribe_persons = True
        self._tracked_persons_subscriber = None
        self._known_persons_subscriber = None
        self._tracked_persons_callbacks = []
        self._known_persons_callbacks = []
        self.tracked_persons = EntityDict()
        self.known_persons = EntityDict()

        # this lock avoids that a person is creates / deleted one of the person dictionaries while the other tries to search in it
        self._persons_lock = multiprocessing.Lock()

        # BODIES
        self._subscribe_bodies = subscribe_bodies
        self._body_property_descriptions = self._get_property_descriptions_from_input(defaults.body_property_descriptions, body_properties)
        self._body_property_callbacks = []
        if self._body_property_descriptions:
            self._subscribe_bodies = True
        self._tracked_bodies_subscriber = None
        self._tracked_bodies_callbacks = []
        self.bodies = EntityDict()

        # FACES
        self._subscribe_faces = subscribe_faces
        self._face_property_descriptions = self._get_property_descriptions_from_input(defaults.face_property_descriptions, face_properties)
        self._face_property_callbacks = []
        if self._face_property_descriptions:
            self._subscribe_faces = True
        self._tracked_faces_subscriber = None
        self._tracked_faces_callbacks = []
        self.faces = EntityDict()

        # VOICES
        self._subscribe_voices = subscribe_voices
        self._voice_property_descriptions = self._get_property_descriptions_from_input(defaults.voice_property_descriptions, voice_properties)
        self._voice_property_callbacks = []
        if self._voice_property_descriptions:
            self._subscribe_voices = True
        self._tracked_voices_subscriber = None
        self._tracked_voices_callbacks = []
        self.voices = EntityDict()

        # GROUPS
        self._subscribe_groups = subscribe_groups
        self._group_property_descriptions = self._get_property_descriptions_from_input(defaults.group_property_descriptions, group_properties)
        self._group_property_callbacks = []
        if self._group_property_descriptions:
            self._subscribe_groups = True
        self._tracked_groups_subscriber = None
        self._tracked_groups_callbacks = []
        self.groups = EntityDict()


    def _start(self, sleep=0.5):
        self._started = True

        if self._subscribe_persons:
            self._tracked_persons_subscriber = rospy.Subscriber(
                defaults.person_topic_namespace + 'tracked',
                IdsList,
                self._tracked_persons_callback
            )

            self._known_persons_subscriber = rospy.Subscriber(
                defaults.person_topic_namespace + 'known',
                IdsList,
                self._known_persons_callback
            )

        if self._subscribe_bodies:
            self._tracked_bodies_subscriber = rospy.Subscriber(
                defaults.body_topic_namespace + 'tracked',
                IdsList,
                self._tracked_bodies_callback
            )

        if self._subscribe_faces:
            self._tracked_faces_subscriber = rospy.Subscriber(
                defaults.face_topic_namespace + 'tracked',
                IdsList,
                self._tracked_faces_callback
            )

        if self._subscribe_voices:
            self._tracked_voices_subscriber = rospy.Subscriber(
                defaults.voice_topic_namespace + 'tracked',
                IdsList,
                self._tracked_voices_callback
            )

        if self._subscribe_groups:
            self._tracked_groups_subscriber = rospy.Subscriber(
                defaults.group_topic_namespace + 'tracked',
                IdsList,
                self._tracked_groups_callback
            )

        # sleep to give enough time to register the publishers / subscriber
        rospy.sleep(sleep)


    ####################################
    # add properties

    def add_group_property(self, name, msg_type=None, data_function=None, callback_function=None):
        """Adds a property of groups that should be subscribed.

        For some properties exists default configurations (message type, data function):
            - 'members' (IDsList, data: msg.ids)

        Args:
            name (string): Name of the property (Example: 'members').
            msg_type:
            data_function: Optional function to collect the data from the subscribed ros message.
        """
        if self.started:
            raise ValueError('Can not add properties after subscriber is started!')

        if name in ['member_ids', 'members']:
            raise ValueError('Can not define custom properties for member_ids or members of a group!')

        self._subscribe_groups = True

        self._add_entity_property(defaults.group_property_descriptions, self._group_property_descriptions, name, msg_type, data_function)

        if callback_function:
            self.add_group_property_callback(name, callback_function)


    def add_person_property(self, name, msg_type=None, data_function=None, callback_function=None):

        if self.started:
            raise ValueError('Can not add properties after subscriber is started!')

        if name in ['face_id', 'body_id', 'voice_id', 'face', 'body', 'voice']:
            raise ValueError('Can not define custom properties for the face, body, or voice of a person!')

        self._subscribe_persons = True

        self._add_entity_property(defaults.person_property_descriptions, self._person_property_descriptions, name, msg_type, data_function)

        if callback_function:
            self.add_person_property_callback(name, callback_function)


    def add_body_property(self, name, msg_type=None, data_function=None, callback_function=None):

        if self.started:
            raise ValueError('Can not add properties after subscriber is started!')

        self._subscribe_bodies = True

        self._add_entity_property(defaults.body_property_descriptions, self._body_property_descriptions, name, msg_type, data_function)

        if callback_function:
            self.add_body_property_callback(name, callback_function)


    def add_face_property(self, name, msg_type=None, data_function=None, callback_function=None):

        if self.started:
            raise ValueError('Can not add properties after subscriber is started!')

        self._subscribe_faces = True

        self._add_entity_property(defaults.face_property_descriptions, self._face_property_descriptions, name, msg_type, data_function)

        if callback_function:
            self.add_face_property_callback(name, callback_function)


    def add_voice_property(self, name, msg_type=None, data_function=None, callback_function=None):

        if self.started:
            raise ValueError('Can not add properties after subscriber is started!')

        self._subscribe_voices = True

        self._add_entity_property(defaults.voice_property_descriptions, self._voice_property_descriptions, name, msg_type, data_function)

        if callback_function:
            self.add_voice_property_callback(name, callback_function)


    def _add_entity_property(self, default_property_descriptions, property_descriptions, name, msg_type=None, data_function=None):

        if msg_type is None and data_function is not None:
            raise ValueError('Defining no msg_type but a data_function is not possible!')

        if msg_type is not None:
            property_descriptions.append(PropertyDescription(name, msg_type, data_function))
        else:
            # search the name in the default property descriptions
            if name not in default_property_descriptions:
                raise ValueError(
                    'A default property with the name \'{}\' does not exist! Please specify the msg_type and optionally the data_function.'.format(name)
                )
            property_descriptions.append(default_property_descriptions[name])


    ##################################
    # callback registration

    # helper functions

    @staticmethod
    def _find_property_description(descriptions, name):
        """Finds a property description with the given name in the given list of property descriptions. Returns None if not found."""
        descr = None
        for cur_descr in descriptions:
            if name == cur_descr[0]:
                descr = cur_descr
                break
        return descr

    # callbacks for tracked entities

    def add_tracked_persons_callback(self, callback_function):
        self._subscribe_persons = True
        self._tracked_persons_callbacks.append(callback_function)


    def add_known_persons_callback(self, callback_function):
        self._subscribe_persons = True
        self._known_persons_callbacks.append(callback_function)


    def add_tracked_bodies_callback(self, callback_function):
        self._subscribe_bodies = True
        self._tracked_bodies_callbacks.append(callback_function)


    def add_tracked_faces_callback(self, callback_function):
        self._subscribe_faces = True
        self._tracked_faces_callbacks.append(callback_function)


    def add_tracked_voices_callback(self, callback_function):
        self._subscribe_voices = True
        self._tracked_voices_callbacks.append(callback_function)


    def add_tracked_groups_callback(self, callback_function):
        self._subscribe_groups = True
        self._tracked_groups_callbacks.append(callback_function)

    # callbacks for properties

    def add_person_property_callback(self, name, callback_function):

        if self._find_property_description(self._person_property_descriptions, name) is None:
            raise ValueError('Callbacks can only be added for properties that are subscribed!')

        self._person_property_callbacks.append((name, callback_function))


    def add_body_property_callback(self, name, callback_function):

        if self._find_property_description(self._body_property_descriptions, name) is None:
            raise ValueError('Callbacks can only be added for properties that are subscribed!')

        self._body_property_callbacks.append((name, callback_function))


    def add_face_property_callback(self, name, callback_function):

        if self._find_property_description(self._face_property_descriptions, name) is None:
            raise ValueError('Callbacks can only be added for properties that are subscribed!')

        self._face_property_callbacks.append((name, callback_function))


    def add_voice_property_callback(self, name, callback_function):

        if self._find_property_description(self._voice_property_descriptions, name) is None:
            raise ValueError('Callbacks can only be added for properties that are subscribed!')

        self._voice_property_callbacks.append((name, callback_function))


    def add_group_property_callback(self, name, callback_function):

        if self._find_property_description(self._group_property_descriptions, name) is None:
            raise ValueError('Callbacks can only be added for properties that are subscribed!')

        self._group_property_callbacks.append((name, callback_function))


    ##################################
    # copy methods

    def copy_tracked_persons(self):
        return self._copy_tracker(self.tracked_persons)


    def copy_known_persons(self):
        return self._copy_tracker(self.known_persons)


    def copy_bodies(self):
        return self._copy_tracker(self.bodies)


    def copy_faces(self):
        return self._copy_tracker(self.faces)


    def copy_voices(self):
        return self._copy_tracker(self.voices)


    def copy_groups(self):
        return self._copy_tracker(self.groups)


    def _copy_tracker(self, tracker):
        """Creates a copy of the given dictionary that keeps subscribers of its objects"""
        with tracker.lock:
            return copy.copy(tracker)


    ##################################
    # data copy methods

    def copy_tracked_persons_data(self):
        return self._copy_tracker_data(self.tracked_persons)


    def copy_known_persons_data(self):
        return self._copy_tracker_data(self.known_persons)


    def copy_bodies_data(self):
        return self._copy_tracker_data(self.bodies)


    def copy_faces_data(self):
        return self._copy_tracker_data(self.faces)


    def copy_voices_data(self):
        return self._copy_tracker_data(self.voices)


    def copy_groups_data(self):
        return self._copy_tracker_data(self.groups)


    def _copy_tracker_data(self, tracker):
        """Creates a copy of the given dictionary that copies data"""
        with tracker.lock:
            copied_dict = dict()
            for entity_id, entity in tracker.items():
                copied_dict[entity_id] = entity._copy_data()

        return copied_dict


    ###################################
    # data deep copy

    def deepcopy_tracked_persons_data(self):
        return self._deepcopy_tracker_data(self.tracked_persons)


    def deepcopy_known_persons_data(self):
        return self._deepcopy_tracker_data(self.known_persons)


    def deepcopy_bodies_data(self):
        return self._deepcopy_tracker_data(self.bodies)


    def deepcopy_faces_data(self):
        return self._deepcopy_tracker_data(self.faces)


    def deepcopy_voices_data(self):
        return self._deepcopy_tracker_data(self.voices)


    def deepcopy_groups_data(self):
        return self._deepcopy_tracker_data(self.groups)


    def _deepcopy_tracker_data(self, tracker):
        with tracker.lock:
            copied_dict = dict()
            for entity_id, entity in tracker.items():
                copied_dict[entity_id] = copy.deepcopy(entity)

        return copied_dict


    ####################################
    # tracker callbacks and update of associated dictionaries

    def _tracked_persons_callback(self, msg):
        self._update_entity_dict(self.tracked_persons, Person, self._person_property_descriptions, self._person_property_callbacks, msg)

        for callback_function in self._tracked_persons_callbacks:
            callback_function(msg)


    def _known_persons_callback(self, msg):
        self._update_entity_dict(self.known_persons, Person, self._person_property_descriptions, self._person_property_callbacks, msg)

        for callback_function in self._known_persons_callbacks:
            callback_function(msg)


    def _tracked_bodies_callback(self, msg):
        self._update_entity_dict(self.bodies, Body, self._body_property_descriptions, self._body_property_callbacks, msg)

        for callback_function in self._tracked_bodies_callbacks:
            callback_function(msg)


    def _tracked_faces_callback(self, msg):
        self._update_entity_dict(self.faces, Face, self._face_property_descriptions, self._face_property_callbacks, msg)

        for callback_function in self._tracked_faces_callbacks:
            callback_function(msg)


    def _tracked_voices_callback(self, msg):
        self._update_entity_dict(self.voices, Voice, self._voice_property_descriptions, self._voice_property_callbacks, msg)

        for callback_function in self._tracked_voices_callbacks:
            callback_function(msg)


    def _tracked_groups_callback(self, msg):
        self._update_entity_dict(self.groups, Group, self._group_property_descriptions, self._group_property_callbacks, msg)

        for callback_function in self._tracked_groups_callbacks:
            callback_function(msg)


    def _update_entity_dict(self, entity_dict, entity_class, property_descriptions, property_callbacks, new_ids_msg):

        new_header = new_ids_msg.header
        new_ids = new_ids_msg.ids

        with entity_dict.lock:

            entity_dict._header = new_header

            with self._persons_lock:

                new_ids = set(new_ids)
                current_ids = set(entity_dict.keys())

                to_remove = current_ids - new_ids
                to_add = new_ids - current_ids

                for id in to_remove:
                    if entity_dict != self.tracked_persons or id not in self.known_persons:
                        # do not close untracked persons, if they are still in the known_persons list
                        entity_dict[id].close()
                    del entity_dict[id]

                for id in to_add:

                    if entity_class == Person:
                        # if person exists already in known_persons or tracked_persons, then get it from there instead of creating a new object
                        if entity_dict == self.tracked_persons:
                            if id in self.known_persons:
                                entity_dict[id] = self.known_persons[id]
                            else:
                                entity_dict[id] = entity_class(self, id, property_descriptions, property_callbacks)
                        else:
                            if id in self.tracked_persons:
                                entity_dict[id] = self.tracked_persons[id]
                            else:
                                entity_dict[id] = entity_class(self, id, property_descriptions, property_callbacks)
                    else:
                        entity_dict[id] = entity_class(self, id, property_descriptions, property_callbacks)


    ###############################

    def close(self):
        if self.started:

            # remove all callbacks for tracked entities
            self._tracked_persons_callbacks.clear()
            self._known_persons_callbacks.clear()
            self._tracked_bodies_callbacks.clear()
            self._tracked_faces_callbacks.clear()
            self._tracked_voices_callbacks.clear()
            self._tracked_groups_callbacks.clear()

            if self._subscribe_persons:
                self._tracked_persons_subscriber.unregister()
                self._known_persons_subscriber.unregister()
                with self.tracked_persons.lock:
                    for person in self.tracked_persons.values():
                        person.close()
                with self.known_persons.lock:
                    for person in self.known_persons.values():
                        person.close()

            if self._subscribe_bodies:
                self._tracked_bodies_subscriber.unregister()
                with self.bodies.lock:
                    for body in self.bodies.values():
                        body.close()

            if self._subscribe_faces:
                self._tracked_faces_subscriber.unregister()
                with self.faces.lock:
                    for face in self.faces.values():
                        face.close()

            if self._subscribe_voices:
                self._tracked_voices_subscriber.unregister()
                with self.voices.lock:
                    for voice in self.voices.values():
                        voice.close()

            if self._subscribe_groups:
                self._tracked_groups_subscriber.unregister()
                with self.groups.lock:
                    for group in self.groups.values():
                        group.close()
