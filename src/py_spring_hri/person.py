import rospy
import copy
from std_msgs.msg import String, Bool, Float32
from .entity import Entity
from .defaults import defaults


class Person(Entity):

    @property
    def body_id(self):
        return self._body_id

    @property
    def body(self):
        return self._body


    @property
    def face_id(self):
        return self._face_id

    @property
    def face(self):
        return self._face


    @property
    def voice_id(self):
        return self._voice_id

    @property
    def voice(self):
        return self._voice


    def __init__(self, hri, id, property_descriptions=None, callback_functions=None, is_data_copy=False):

        self._topic_namespace = defaults.person_topic_namespace
        self._tf_template = defaults.person_tf_template

        super().__init__(hri, id, property_descriptions, callback_functions=callback_functions, is_data_copy=is_data_copy)

        self._body_id = None
        self._body = None
        self._face_id = None
        self._face = None
        self._voice_id = None
        self._voice = None

        if is_data_copy:
            self._body_id_sub = None
            self._face_id_sub = None
            self._voice_id_sub = None
        else:
            # create specific properties for body, face, and voice
            self._body_id_sub = rospy.Subscriber(
                self._topic_namespace + '{}/body_id'.format(self.id), String, self._on_body_id
            )
            self._face_id_sub = rospy.Subscriber(
                self._topic_namespace + '{}/face_id'.format(self.id), String, self._on_face_id
            )
            self._voice_id_sub = rospy.Subscriber(
                self._topic_namespace + '{}/voice_id'.format(self.id), String, self._on_voice_id
            )


    def _on_body_id(self, msg):
        self._body_id = msg.data

        if self._hri.subscribe_bodies:
            with self._hri.bodies.lock:
                if self.body_id:
                    self._body = self._hri.bodies[self.body_id]
                else:
                    self._body = None


    def _on_face_id(self, msg):
        self._face_id = msg.data

        if self._hri.subscribe_faces:
            with self._hri.faces.lock:
                if self.face_id:
                    self._face = self._hri.faces[self.face_id]
                else:
                    self._face = None


    def _on_voice_id(self, msg):
        self._voice_id = msg.data

        if self._hri.subscribe_voices:
            with self._hri.voices.lock:
                if self.voice_id:
                    self._voice = self._hri.voices[self._voice_id]
                else:
                    self._voice = None


    def _copy_data(self):

        copied = super()._copy_data()

        copied._body_id = self.body_id
        copied._face_id = self.face_id
        copied._voice_id = self.voice_id

        if self.body is not None:
            copied._body = self.body._copy_data()
        if self.face is not None:
            copied._face = self.face._copy_data()
        if self.voice is not None:
            copied._voice = self.voice._copy_data()

        return copied


    def __deepcopy__(self, memo):

        copied = super().__deepcopy__(memo)

        copied._body_id = copy.deepcopy(self.body_id)
        copied._face_id = copy.deepcopy(self.face_id)
        copied._voice_id = copy.deepcopy(self.voice_id)

        if self.body is not None:
            copied._body = copy.deepcopy(self.body)
        if self.face is not None:
            copied._face = copy.deepcopy(self.face)
        if self.voice is not None:
            copied._voice = copy.deepcopy(self.voice)

        return copied


    def close(self):
        super().close()

        if self._body_id_sub:
            self._body_id_sub.unregister()
        if self._face_id_sub:
            self._face_id_sub.unregister()
        if self._voice_id_sub:
            self._voice_id_sub.unregister()
