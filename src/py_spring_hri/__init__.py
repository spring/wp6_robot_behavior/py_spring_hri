from .person import Person
from .body import Body
from .group import Group
from .hri_listener import HRIListener
from .misc import PropertyDescription
from .defaults import defaults

__version__ = '0.0.3'




