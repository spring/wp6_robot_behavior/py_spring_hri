import rospy
import copy
from .misc import Cv2Image

class EntityPropertySubscriber:
    """
    Subscribes to a property of an entity, for example, the members of a group (/humans/interactions/groups/<group_id>/members).
    """

    @property
    def data(self):
        """Current data of the property."""
        return self._data


    def __init__(self, hri, entity, topic, msg_type, data_function=None, data=None, is_data_copy=False):

        self._data = data
        self._callback_functions = []
        self._entity = entity
        if is_data_copy:
            # create a deep copy that has only the data
            self._tf_buffer = None
            self._cv_bridge = None
            self._subscriber = None
            self._data_function = None
        else:
            self._tf_buffer = hri._tf_buffer
            self._cv_bridge = hri._cv_bridge
            self._subscriber = rospy.Subscriber(
                topic,
                msg_type,
                self._callback
            )

            if data_function is None:
                self._data_function = lambda msg: msg
            elif isinstance(data_function, str):

                if len(data_function) >= 2 and data_function.lower()[0:2] == 'tf':

                    if len(data_function) == 2:
                        # no reference frame is defined --> use default
                        reference_frame = hri._default_reference_frame
                    else:
                        # ignore the ':' that comes after 'tf:<reference_frame>'
                        reference_frame = data_function[3:]

                    self._data_function = lambda msg: self._data_function_tf(reference_frame, msg)

                elif data_function.lower() == 'imgmsg_to_cv2':
                    self._data_function = self._data_function_imgmsg_to_cv2

                else:
                    raise ValueError('Unknown data function \'{}\'!'.format(data_function))
            else:
                self._data_function = data_function


    def _callback(self, msg):
        self._data = self._data_function(msg)

        for func in self._callback_functions:
            func(self._entity, self._data, msg)


    def _copy_data(self):
        return EntityPropertySubscriber(
            hri=None,
            entity=None,
            topic=None,
            msg_type=None,
            data_function=None,
            data=self.data,
            is_data_copy=True
        )


    def __deepcopy__(self, memo):
        # deep copy the entity by only copying the data and nothing else as subscribers can not be deep copied
        return EntityPropertySubscriber(
            hri=None,
            entity=None,
            topic=None,
            msg_type=None,
            data_function=None,
            data=copy.deepcopy(self.data),
            is_data_copy=True
        )


    def register_callback(self, callback_function):
        self._callback_functions.append(callback_function)


    def close(self):
        if self._subscriber:
            self._subscriber.unregister()

    ##################################
    # Default data functions

    def _data_function_tf(self, reference_frame, msg):

        # try:
        trans = self._tf_buffer.lookup_transform(
            msg.data,
            reference_frame,
            rospy.Time(0)
        )
        # TODO: should I ignore certain exceptions?
        # except (tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
        #     trans = self._data
        return trans


    def _data_function_imgmsg_to_cv2(self, msg):
        return Cv2Image(
            msg.header,
            msg.height,
            msg.width,
            self._cv_bridge.imgmsg_to_cv2(msg, desired_encoding='passthrough')
        )
