from collections import namedtuple
import multiprocessing
import copy


PropertyDescription = namedtuple('PropertyDescription', ['name', 'msg_type', 'data_function'])

Cv2Image = namedtuple('Cv2Image', ['header', 'height', 'width', 'image'])


class EntityDict(dict):
    """A dictionary that has a lock to inform pyhri to not modify (add or remove elements) of the dict while it is used.
    Furthermore, it has a header property that stores the header of the associate tracked IDs message.
    """

    @property
    def lock(self):
        return self._lock


    @property
    def header(self):
        return self._header


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._lock = multiprocessing.Lock()
        self._header = None


    def __deepcopy__(self, memo):
        # return a deepcopy of itself but without the lock which can not be deep copied

        lock_buf = self._lock
        self._lock = None
        deepcopied_dict = copy.deepcopy(self, memo)
        self._lock = lock_buf
        return deepcopied_dict
