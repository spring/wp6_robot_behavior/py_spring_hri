from .entity import Entity
from .defaults import defaults


class Face(Entity):

    # # topic namespace for groups
    # TOPIC_NAMESPACE = 'humans/faces/'
    #
    # # default properties that can be defined
    # DEFAULT_PROPERTY_DESCRIPTIONS = [
    #     PropertyDescription('roi', RegionOfInterest, lambda msg: msg),
    #     PropertyDescription('cropped', Image, 'imgmsg_to_cv2'),
    #     PropertyDescription('aligned', Image, 'imgmsg_to_cv2'),
    #     PropertyDescription('frontalized', Image, 'imgmsg_to_cv2'),
    #     PropertyDescription('landmarks', FacialLandmarks, lambda msg: msg),
    #     PropertyDescription('facs', FacialActionUnits, lambda msg: msg),
    #     PropertyDescription('expression', Expression, lambda msg: msg),
    #     PropertyDescription('softbiometrics', SoftBiometrics, lambda msg: msg),
    # ]
    #
    # # default prefix before the tf of a group
    # TF_TEMPLATE = 'face_{}'
    #
    # TF_GAZE_TEMPLATE = 'gaze_{}'
    #
    # TF_FOCUS_TEMPLATE = 'focus_{}'

    def __init__(self, hri, id, property_descriptions=None, callback_functions=None, is_data_copy=False):
        self._topic_namespace = defaults.face_topic_namespace
        self._tf_template = defaults.face_tf_template
        self._tf_gaze_template = defaults.gaze_tf_template
        self._tf_focus_template = defaults.focus_tf_template

        super().__init__(hri, id, property_descriptions, callback_functions=callback_functions, is_data_copy=is_data_copy)


    def transform_gaze(self, target_frame=None, tf_template=None, time=None, time_out=None):
        if tf_template is None:
            tf_template = self._tf_gaze_template
        return self.transform(target_frame=target_frame, tf_template=tf_template, time=time, time_out=time_out)


    def transform_focus(self, target_frame=None, tf_template=None, time=None, time_out=None):
        if tf_template is None:
            tf_template = self._tf_focus_template
        return self.transform(target_frame=target_frame, tf_template=tf_template, time=time, time_out=time_out)

