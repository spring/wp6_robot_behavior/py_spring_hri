import rospy
import copy
from .entity_property_subscriber import EntityPropertySubscriber
from geometry_msgs.msg import TransformStamped
from tf2_ros import LookupException

TF_TIMEOUT = rospy.Duration(0.01)

class Entity:
    """
    Parent class of ROS4HRI entities: Body, Face, Voice, Person, Group
    """

    # DEFAULT_PROPERTY_DESCRIPTIONS = []

    # TOPIC_NAMESPACE = ''
    #
    # TF_TEMPLATE = '{}'

    @property
    def id(self):
        return self._id


    def __init__(self, hri, id, property_descriptions=None, callback_functions=None, is_data_copy=False):

        if callback_functions is None:
            callback_functions = []

        self._id = id
        if is_data_copy:
            self._hri = None
            self._property_subscribers = dict()
        else:
            self._hri = hri

            # create the property subscribers
            self._property_subscribers = dict()

            if property_descriptions:
                for property_description in property_descriptions:
                    self.set_property(
                        property_description.name,
                        property_description.msg_type,
                        property_description.data_function,
                    )

            # register callback functions
            for (prop_name, callback_function) in callback_functions:
                self.add_property_callback(prop_name, callback_function)


    def set_property(self, name, msg_type, data_function=None):
        """Adds a property to the ROS4HRI entity (Group, ...) that should be subscribed.

        Args:
            name (string): Name of the property.
            msg_type:
            data_function: Optional function to collect the data from the subscribed ros message.
        """

        self._property_subscribers[name] = EntityPropertySubscriber(
            self._hri,
            self,
            self._topic_namespace + '{}/{}'.format(self.id, name), # topic, for example /humans/interactions/groups/<group_id>/members
            msg_type,
            data_function
        )


    def add_property_callback(self, name, callback_function):

        if name not in self._property_subscribers:
            raise KeyError('Property {} does not exist!'.format(name))

        self._property_subscribers[name].register_callback(callback_function)


    def close(self):
        for prop_subs in self._property_subscribers.values():
            prop_subs.close()


    def _copy_data(self):

        copied = self.__class__(hri=None, id=self.id, property_descriptions=None, is_data_copy=True)

        for p_name, property_subscriber in self._property_subscribers.items():
            copied._property_subscribers[p_name] = property_subscriber._copy_data()

        return copied


    def __deepcopy__(self, memo):

        copied = self.__class__(hri=None, id=self.id, property_descriptions=None, is_data_copy=True)

        for p_name, property_subscriber in self._property_subscribers.items():
            copied._property_subscribers[p_name] = copy.deepcopy(property_subscriber)

        return copied


    def __getattr__(self, name):

        # avoid to override any missing internal class methods such as __deepcopy__
        if name.startswith('__'):
            raise AttributeError(name)

        return self._property_subscribers[name].data


    def __str__(self):
        return self.id


    def transform(self, target_frame=None, tf_template=None, time=None, time_out=None):

        if target_frame is None:
            target_frame = self._hri.default_target_frame

        if tf_template is None:
            tf_template = self._tf_template

        if time is None:
            time = rospy.Time(0)

        if time_out is None:
            time_out = TF_TIMEOUT

        entity_frame = tf_template.format(self.id)

        try:
            return self._hri._tf_buffer.lookup_transform(target_frame, entity_frame, time, time_out)

        except LookupException:
            rospy.logwarn('Failed to transform frame {} to {}. Are the frames published?'. format(entity_frame, target_frame))
            return None


