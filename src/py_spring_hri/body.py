from .entity import Entity
from .defaults import defaults


class Body(Entity):

    # # topic namespace for groups
    # TOPIC_NAMESPACE = 'humans/bodies/'
    #
    # # default properties that can be defined
    # DEFAULT_PROPERTY_DESCRIPTIONS = [
    #     PropertyDescription('roi', RegionOfInterest, lambda msg: msg),
    #     PropertyDescription('cropped', Image, 'imgmsg_to_cv2'),
    #     PropertyDescription('skeleton2d', Skeleton2D, lambda msg: msg),
    #     PropertyDescription('joint_states', JointState, lambda msg: msg),
    #     PropertyDescription('posture', BodyPosture, lambda msg: msg),
    #     PropertyDescription('gesture', Gesture, lambda msg: msg),
    # ]
    #
    # # default tf template
    # TF_TEMPLATE = 'body_{}'


    def __init__(self, hri, id, property_descriptions=None, callback_functions=None, is_data_copy=False):
        self._topic_namespace = defaults.body_topic_namespace
        self._tf_template = defaults.body_tf_template

        super().__init__(hri, id, property_descriptions, callback_functions=callback_functions, is_data_copy=is_data_copy)
