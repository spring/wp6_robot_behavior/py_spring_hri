import multiprocessing
import copy


class LockDict(dict):
    """A dictionary that has a lock to inform pyhri to not modify (add or remove elements) of the dict while it is used."""

    @property
    def lock(self):
        return self._lock


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._lock = multiprocessing.Lock()


    def __deepcopy__(self, memo):
        # return a deepcopy of itself but without the lock which can not be deep copied
        return copy.deepcopy(dict(self), memo)