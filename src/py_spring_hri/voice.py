#from audio_common_msgs.msg import AudioData
#from hri_msgs.msg import AudioFeatures, LiveSpeech
#from hri_msgs.msg import LiveSpeech
from .entity import Entity
from .defaults import defaults

# TODO: include the audio_common_msgs package for audio data
# TODO: include the newest hri_msgs package!

class Voice(Entity):

    # # topic namespace for groups
    # TOPIC_NAMESPACE = 'humans/voices/'
    #
    # # default properties that can be defined
    # DEFAULT_PROPERTY_DESCRIPTIONS = [
    #     #PropertyDescription('audio', AudioData, lambda msg: msg),
    #     #PropertyDescription('features', AudioFeatures, lambda msg: msg),
    #     PropertyDescription('is_speaking', Bool, lambda msg: msg.data),
    #     #PropertyDescription('speech', LiveSpeech, lambda msg: msg.data),
    # ]
    #
    # # default prefix before the tf of a group
    # TF_TEMPLATE = 'voice_{}'


    def __init__(self, hri, id, property_descriptions=None, callback_functions=None, is_data_copy=False):
        self._topic_namespace = defaults.voice_topic_namespace
        self._tf_template = defaults.voice_tf_template

        super().__init__(hri, id, property_descriptions, callback_functions=callback_functions, is_data_copy=is_data_copy)
