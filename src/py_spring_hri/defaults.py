from .misc import PropertyDescription
from hri_msgs.msg import IdsList, EngagementLevel, Skeleton2D, BodyPosture, Gesture, FacialLandmarks, SoftBiometrics, FacialActionUnits, Expression, AudioFeatures, LiveSpeech, NormalizedRegionOfInterest2D
from std_msgs.msg import String, Bool, Float32
from sensor_msgs.msg import RegionOfInterest, Image, JointState
from audio_common_msgs.msg import AudioData


class defaults:

    # topic namespace for groups
    body_topic_namespace = 'humans/bodies/'
    face_topic_namespace = 'humans/faces/'
    voice_topic_namespace = 'humans/voices/'
    person_topic_namespace = 'humans/persons/'
    group_topic_namespace = 'humans/groups/'

    # default properties that can be defined per entity
    body_property_descriptions = {
        'roi': PropertyDescription('roi', RegionOfInterest, lambda msg: msg),
        'cropped': PropertyDescription('cropped', Image, 'imgmsg_to_cv2'),
        'skeleton2d': PropertyDescription('skeleton2d', Skeleton2D, lambda msg: msg),
        'joint_states': PropertyDescription('joint_states', JointState, lambda msg: msg),
        'posture': PropertyDescription('posture', BodyPosture, lambda msg: msg),
        'gesture': PropertyDescription('gesture', Gesture, lambda msg: msg),
    }

    face_property_descriptions = {
        'roi': PropertyDescription('roi', NormalizedRegionOfInterest2D, lambda msg: msg),
        'cropped': PropertyDescription('cropped', Image, 'imgmsg_to_cv2'),
        'aligned': PropertyDescription('aligned', Image, 'imgmsg_to_cv2'),
        'frontalized': PropertyDescription('frontalized', Image, 'imgmsg_to_cv2'),
        'landmarks': PropertyDescription('landmarks', FacialLandmarks, lambda msg: msg),
        'facs': PropertyDescription('facs', FacialActionUnits, lambda msg: msg),
        'expression': PropertyDescription('expression', Expression, lambda msg: msg),
        'softbiometrics': PropertyDescription('softbiometrics', SoftBiometrics, lambda msg: msg),
    }

    voice_property_descriptions = {
        'audio': PropertyDescription('audio', AudioData, lambda msg: msg.data),
        'features': PropertyDescription('features', AudioFeatures, lambda msg: msg),
        'is_speaking': PropertyDescription('is_speaking', Bool, lambda msg: msg.data),
        'speech': PropertyDescription('speech', LiveSpeech, lambda msg: msg),
    }

    person_property_descriptions = {
        'anonymous': PropertyDescription('anonymous', Bool, lambda msg: msg.data),
        'alias': PropertyDescription('alias', String, lambda msg: msg.data),
        'engagement_status': PropertyDescription('engagement_status', EngagementLevel, lambda msg: msg),
        'location_confidence': PropertyDescription('location_confidence', Float32, lambda msg: msg.data),
        'name': PropertyDescription('name', String, lambda msg: msg.data),
        'native_language': PropertyDescription('native_language', String, lambda msg: msg.data),
    }

    group_property_descriptions = {
        # 'members': PropertyDescription('members', IdsList, lambda msg: msg.ids)
    }

    # templates for tfs of the various entities
    body_tf_template = 'body_{}'
    face_tf_template = 'face_{}'
    gaze_tf_template = 'gaze_{}'
    focus_tf_template = 'focus_{}'
    voice_tf_template = 'voice_{}'
    person_tf_template = 'person_{}'
    group_tf_template = 'group_{}'

