import rospy
from hri_msgs.msg import IdsList
from .entity import Entity
from .defaults import defaults


class Group(Entity):

    # TODO: create a list of members that are the persons

    @property
    def member_ids(self):
        return self._member_ids

    @property
    def members(self):
        return self._members


    def __init__(self, hri, id, property_descriptions=None, callback_functions=None, is_data_copy=False):

        self._topic_namespace = defaults.group_topic_namespace
        self._tf_template = defaults.group_tf_template

        super().__init__(hri, id, property_descriptions, callback_functions=callback_functions, is_data_copy=is_data_copy)

        self._member_ids = None
        self._members = None

        if is_data_copy:
            self._member_ids_sub = None
        else:
            # create specific properties for body, face, and voice
            self._member_ids_sub = rospy.Subscriber(
                self._topic_namespace + '{}/member_ids'.format(self.id), IdsList, self._on_member_ids
            )


    def _on_member_ids(self, msg):
        self._member_ids = msg.ids

        # get a list of members as objects of persons if persons are tracked
        if self._hri.subscribe_persons:
            members = dict()

            with self._hri._persons_lock:
                for person_id in self._member_ids:
                    if person_id in self._hri.tracked_persons:
                        members[person_id] = self._hri.tracked_persons[person_id]
                    elif person_id in self._hri.known_persons:
                        members[person_id] = self._hri.known_persons[person_id]
                    else:
                        rospy.logwarn('Person {} is in group {} but seems not to be tracked or known!'.format(person_id, self.id))

            self._members = members

    
    def close(self):
        super().close()

        if self._member_ids_sub:
            self._member_ids_sub.unregister()