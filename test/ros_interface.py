import rospy
import tf
from cv_bridge import CvBridge
import numpy as np
from collections import namedtuple
from hri_msgs.msg import IdsList, EngagementLevel, FacialLandmarks, SoftBiometrics, FacialActionUnits, Expression, LiveSpeech, AudioFeatures
from audio_common_msgs.msg import AudioData
from std_msgs.msg import String, Bool
from sensor_msgs.msg import Image, RegionOfInterest


GROUPS_TOPIC_NAMESPACE = '/humans/groups/'
BODIES_TOPIC_NAMESPACE = '/humans/bodies/'
FACES_TOPIC_NAMESPACE = '/humans/faces/'
VOICES_TOPIC_NAMESPACE = '/humans/voices/'
PERSONS_TOPIC_NAMESPACE = '/humans/persons/'


class ROSInterface:

    def __init__(self):

        self._bodies_tracked_publisher = rospy.Publisher(BODIES_TOPIC_NAMESPACE + 'tracked', IdsList, queue_size=1, latch=False)
        self._body_publishers = dict()

        self._faces_tracked_publisher = rospy.Publisher(FACES_TOPIC_NAMESPACE + 'tracked', IdsList, queue_size=1, latch=False)
        self._face_publishers = dict()

        self._voices_tracked_publisher = rospy.Publisher(VOICES_TOPIC_NAMESPACE + 'tracked', IdsList, queue_size=1, latch=False)
        self._voice_publishers = dict()

        self._groups_tracked_publisher = rospy.Publisher(GROUPS_TOPIC_NAMESPACE + 'tracked', IdsList, queue_size=1, latch=False)
        self._group_publishers = dict()

        self._persons_tracked_publisher = rospy.Publisher(PERSONS_TOPIC_NAMESPACE + 'tracked', IdsList, queue_size=1, latch=False)
        self._persons_known_publisher = rospy.Publisher(PERSONS_TOPIC_NAMESPACE + 'known', IdsList, queue_size=1, latch=False)
        self._person_publishers = dict()

        self._group_tf_publishers = dict()
        self.tf_broadcaster = tf.TransformBroadcaster()

        self._cv_bridge = CvBridge()

        # sleep to allow correct registration of publishers and subscribers
        rospy.sleep(1)


    def publish_group_tf(self, group_id, tf):

        if group_id in self._group_tf_publishers:
            pub = self._group_tf_publishers[group_id]
        else:
            pub = rospy.Publisher(
                GROUPS_TOPIC_NAMESPACE + '{}/tf'.format(group_id),
                String,
                queue_size=1
            )
            self._group_tf_publishers[group_id] = pub
            #rospy.sleep(1.0)

        msg = String()
        msg.data = tf
        pub.publish(msg)


    def publish_tf(self, name, pos=None):

        if pos is None:
            pos = (0.0, 0.0, 0.0, 0.0)

        current_time = rospy.Time.now()
        quat = tf.transformations.quaternion_from_euler(0, 0, pos[3])
        self.tf_broadcaster.sendTransform(
            (pos[0], pos[1], pos[2]),
            quat,
            current_time,
            name,
            'map'
        )


    ###########################
    # BODIES

    BodyPublishers = namedtuple('BodiesPublishers', ['roi', 'cropped'])

    def publish_body(self, body_id='body_1'):

        if body_id not in self._body_publishers:

            roi_pub = rospy.Publisher(
                BODIES_TOPIC_NAMESPACE + '{}/roi'.format(body_id),
                RegionOfInterest,
                queue_size=1
            )

            cropped_pub = rospy.Publisher(
                BODIES_TOPIC_NAMESPACE + '{}/cropped'.format(body_id),
                Image,
                queue_size=1
            )

            self._body_publishers[body_id] = ROSInterface.BodyPublishers(
                roi_pub,
                cropped_pub
            )

            rospy.sleep(1)

        pubs = self._body_publishers[body_id]

        # roi
        roi_msg = RegionOfInterest()
        roi_msg.x_offset = 1
        roi_msg.y_offset = 2
        roi_msg.height = 3
        roi_msg.width = 4
        roi_msg.do_rectify = False
        pubs.roi.publish(roi_msg)

        height = 512
        width = 512
        img = np.zeros((height, width, 3), np.uint8)

        cropped_msg = self._cv_bridge.cv2_to_imgmsg(img, 'rgb8')
        pubs.cropped.publish(cropped_msg)

        return body_id

    def publish_tracked_bodies(self, body_ids):
        msg = IdsList()
        msg.ids = body_ids
        self._bodies_tracked_publisher.publish(msg)


    ###########################
    # Faces

    FacePublishers = namedtuple('FacePublishers', ['roi', 'cropped', 'aligned', 'frontalized', 'landmarks', 'facs', 'expression', 'softbiometrics'])

    def publish_face(self, face_id='face_1', roi=(1, 2, 3, 4), cropped=(128, 128), aligned=(64, 64), frontalized=(32, 32), landmarks=[1, 2, 3, 4],
                     facs=None, expression='neutral', softbiometrics=(30, 0)):

        if face_id not in self._face_publishers:
            self._face_publishers[face_id] = ROSInterface.FacePublishers(
                rospy.Publisher(
                    FACES_TOPIC_NAMESPACE + '{}/roi'.format(face_id),
                    RegionOfInterest,
                    queue_size=1
                ),
                rospy.Publisher(
                    FACES_TOPIC_NAMESPACE + '{}/cropped'.format(face_id),
                    Image,
                    queue_size=1
                ),
                rospy.Publisher(
                    FACES_TOPIC_NAMESPACE + '{}/aligned'.format(face_id),
                    Image,
                    queue_size=1
                ),
                rospy.Publisher(
                    FACES_TOPIC_NAMESPACE + '{}/frontalized'.format(face_id),
                    Image,
                    queue_size=1
                ),
                rospy.Publisher(
                    FACES_TOPIC_NAMESPACE + '{}/landmarks'.format(face_id),
                    FacialLandmarks,
                    queue_size=1
                ),
                rospy.Publisher(
                    FACES_TOPIC_NAMESPACE + '{}/facs'.format(face_id),
                    FacialActionUnits,
                    queue_size=1
                ),
                rospy.Publisher(
                    FACES_TOPIC_NAMESPACE + '{}/expression'.format(face_id),
                    Expression,
                    queue_size=1
                ),
                rospy.Publisher(
                    FACES_TOPIC_NAMESPACE + '{}/softbiometrics'.format(face_id),
                    SoftBiometrics,
                    queue_size=1
                ),
            )

            rospy.sleep(1)

        pubs = self._face_publishers[face_id]

        # roi
        roi_msg = RegionOfInterest()
        roi_msg.x_offset = roi[0]
        roi_msg.y_offset = roi[1]
        roi_msg.height = roi[2]
        roi_msg.width = roi[3]
        roi_msg.do_rectify = False
        pubs.roi.publish(roi_msg)

        # cropped
        img = np.zeros((cropped[0], cropped[1], 3), np.uint8)
        cropped_msg = self._cv_bridge.cv2_to_imgmsg(img, 'rgb8')
        pubs.cropped.publish(cropped_msg)

        # aligned
        img = np.zeros((aligned[0], aligned[1], 3), np.uint8)
        aligned_msg = self._cv_bridge.cv2_to_imgmsg(img, 'rgb8')
        pubs.aligned.publish(aligned_msg)

        # frontalized
        img = np.zeros((frontalized[0], frontalized[1], 3), np.uint8)
        frontalized_msg = self._cv_bridge.cv2_to_imgmsg(img, 'rgb8')
        pubs.frontalized.publish(frontalized_msg)

        # landmarks
        landmarks_msg = FacialLandmarks()
        landmarks_msg.landmarks = landmarks
        pubs.landmarks.publish(landmarks_msg)

        # facs
        facs_msg = FacialActionUnits()
        pubs.facs.publish(facs_msg)

        # expression
        expression_msg = Expression()
        expression_msg.expression = expression
        pubs.expression.publish(expression_msg)

        # softbiometrics
        softbiometrics_msg = SoftBiometrics()
        softbiometrics_msg.age = softbiometrics[0]
        softbiometrics_msg.age_confidence = 1.0
        softbiometrics_msg.gender = softbiometrics[1]
        softbiometrics_msg.gender_confidence = 1.0
        pubs.softbiometrics.publish(softbiometrics_msg)

        return face_id


    def publish_tracked_faces(self, face_ids):
        msg = IdsList()
        msg.ids = face_ids
        self._faces_tracked_publisher.publish(msg)

    ###########################
    # Voices

    VoicePublishers = namedtuple('VoicePublishers', ['audio', 'features', 'is_speaking', 'speech'])

    def publish_voice(self, voice_id='voice_1', audio=[1,2,3], features=0.0, is_speaking=False, speech='hallo'):

        if voice_id not in self._voice_publishers:
            self._voice_publishers[voice_id] = ROSInterface.VoicePublishers(
                rospy.Publisher(
                    VOICES_TOPIC_NAMESPACE + '{}/audio'.format(voice_id),
                    AudioData,
                    queue_size=1
                ),
                rospy.Publisher(
                    VOICES_TOPIC_NAMESPACE + '{}/features'.format(voice_id),
                    AudioFeatures,
                    queue_size=1
                ),
                rospy.Publisher(
                    VOICES_TOPIC_NAMESPACE + '{}/is_speaking'.format(voice_id),
                    Bool,
                    queue_size=1
                ),
                rospy.Publisher(
                    VOICES_TOPIC_NAMESPACE + '{}/speech'.format(voice_id),
                    LiveSpeech,
                    queue_size=1
                ),
            )

            rospy.sleep(1)

        pubs = self._voice_publishers[voice_id]

        # audio
        audio_msg = AudioData()
        audio_msg.data = audio
        pubs.audio.publish(audio_msg)

        # features
        features_msg = AudioFeatures()
        features_msg.pitch = features
        pubs.features.publish(features_msg)

        # is_speaking
        is_speaking_msg = Bool()
        is_speaking_msg.data = is_speaking
        pubs.is_speaking.publish(is_speaking_msg)

        # speech
        speech_msg = LiveSpeech()
        speech_msg.final = speech
        pubs.speech.publish(speech_msg)

        return voice_id


    def publish_tracked_voices(self, voice_ids):
        msg = IdsList()
        msg.ids = voice_ids
        self._voices_tracked_publisher.publish(msg)


    ###########################
    # PERSONS

    PersonPublishers = namedtuple('PersonPublishers', ['body_id', 'alias', 'engagement_status', 'custom_topic'])

    def publish_person(self, person_id='person_1', body_id='', alias='something', engagement_status=1, custom_topic='[1,2,3,4]'):

        if person_id not in self._person_publishers:

            body_id_pub = rospy.Publisher(
                PERSONS_TOPIC_NAMESPACE + '{}/body_id'.format(person_id),
                String,
                queue_size=1
            )

            alias_pub = rospy.Publisher(
                PERSONS_TOPIC_NAMESPACE + '{}/alias'.format(person_id),
                String,
                queue_size=1
            )

            engaged_pub = rospy.Publisher(
                PERSONS_TOPIC_NAMESPACE + '{}/engagement_status'.format(person_id),
                EngagementLevel,
                queue_size=1
            )

            custom_topic_pub = rospy.Publisher(
                PERSONS_TOPIC_NAMESPACE + '{}/custom_property'.format(person_id),
                String,
                queue_size=1
            )

            self._person_publishers[person_id] = ROSInterface.PersonPublishers(
                body_id_pub,
                alias_pub,
                engaged_pub,
                custom_topic_pub
            )

            rospy.sleep(1)

        pubs = self._person_publishers[person_id]

        # body_id
        body_id_msg = String()
        body_id_msg.data = body_id
        pubs.body_id.publish(body_id_msg)

        # alias
        alias_msg = String()
        alias_msg.data = alias
        pubs.alias.publish(alias_msg)

        # alias
        engagement_status_msg = EngagementLevel()
        engagement_status_msg.level = engagement_status
        pubs.engagement_status.publish(engagement_status_msg)

        # custom topic
        custom_topic_msg = String()
        custom_topic_msg.data = custom_topic
        pubs.custom_topic.publish(custom_topic_msg)

        return person_id


    def publish_tracked_persons(self, person_ids):
        msg = IdsList()
        msg.ids = person_ids
        self._persons_tracked_publisher.publish(msg)


    def publish_known_persons(self, person_ids):
        msg = IdsList()
        msg.ids = person_ids
        self._persons_known_publisher.publish(msg)


    ###########################
    # GROUPS

    GroupPublishers = namedtuple('GroupPublishers', ['member_ids'])

    def publish_group(self, group_id='group_1', member_ids=None):

        if group_id.startswith('group_'):
            group_sub_id = group_id[6:]
        else:
            group_sub_id = group_id

        if member_ids is None:
            members = ['person_{}.{}'.format(group_sub_id, p_idx + 1) for p_idx in range(3)]

        if group_id not in self._group_publishers:

            members_pub = rospy.Publisher(
                GROUPS_TOPIC_NAMESPACE + '{}/member_ids'.format(group_id),
                IdsList,
                queue_size=1
            )

            self._group_publishers[group_id] = ROSInterface.GroupPublishers(
                members_pub
            )

            rospy.sleep(1)

        pubs = self._group_publishers[group_id]

        # members
        msg = IdsList()
        msg.ids = members
        pubs.member_ids.publish(msg)

        return group_id, member_ids


    def publish_tracked_groups(self, group_ids):
        msg = IdsList()
        msg.ids = group_ids
        self._groups_tracked_publisher.publish(msg)