#! /usr/bin/env python
import sys
import unittest
import rostest
import rospy
import threading
import tf
from geometry_msgs.msg import TransformStamped
from std_msgs.msg import String
from hri_msgs.msg import IdsList
from sensor_msgs.msg import RegionOfInterest
import py_spring_hri
from py_spring_hri import HRIListener
from ros_interface import ROSInterface

# TODO: test the different classmethods of the HRIListener
# TODO: test header of entity dictionaries


class PySpringHriInterfaceCase(unittest.TestCase):

    def __init__(self, *args):
        super().__init__(*args)

        rospy.init_node('test_group_hri_interface', anonymous=True)

        self.ros_interface = ROSInterface()

        self._tracked_entities_callback_counter = 0
        self._tracked_entities_callback_args = None
        self._tracked_entity_property_callback_counter = 0
        self._tracked_entity_property_callback_args = None


    def my_group_creation_thread(self):

        # publish an increasing amount of groups and its members
        pub_rate = rospy.Rate(2)
        for i in range(5):

            group_ids = []
            for group_idx in range(i+1):
                group_id = 'group_{}'.format(group_idx)
                self.ros_interface.publish_group(group_id=group_id)
                group_ids.append(group_id)

            self.ros_interface.publish_tracked_groups(group_ids)
            pub_rate.sleep()

        # publish decreasing amount of groups
        pub_rate = rospy.Rate(2)
        for i in range(5-1, 0-1, -1):

            group_ids = []
            for group_idx in range(i+1):
                group_id = 'group_{}'.format(group_idx)
                self.ros_interface.publish_group(group_id=group_id)
                group_ids.append(group_id)

            self.ros_interface.publish_tracked_groups(group_ids)
            pub_rate.sleep()

        self.ros_interface.publish_tracked_groups([])


    def test_set_default_namespaces(self):

        tmp_body_namespace = py_spring_hri.defaults.body_topic_namespace
        tmp_tf_body = py_spring_hri.defaults.body_tf_template

        # register publisher
        my_bodies_tracked_publisher = rospy.Publisher('hri/bodies/tracked', IdsList, queue_size=1, latch=False)
        my_body_property_publisher = rospy.Publisher('hri/bodies/b_1/roi', RegionOfInterest, queue_size=1, latch=False)
        rospy.sleep(1)

        try:
            py_spring_hri.defaults.body_topic_namespace = 'hri/bodies/'
            py_spring_hri.defaults.body_tf_template = 'my_body_{}'

            hri = HRIListener.start_with_default_properties(default_target_frame='map')

            pub_rate = rospy.Rate(5)
            for _ in range(5):
                msg = IdsList()
                msg.ids = ['b_1']
                my_bodies_tracked_publisher.publish(msg)

                msg = RegionOfInterest()
                msg.x_offset = 1
                my_body_property_publisher.publish(msg)

                # publish tf
                self.ros_interface.tf_broadcaster.sendTransform(
                    (1.0, 1.0, 1.0),
                    tf.transformations.quaternion_from_euler(0, 0, 0),
                    rospy.Time.now(),
                    'my_body_b_1',
                    'map'
                )

                pub_rate.sleep()

            with hri.bodies.lock:
                assert len(hri.bodies) == 1
                assert 'b_1' in hri.bodies

                # raise ValueError(hri.bodies['b_1'].roi)
                assert hri.bodies['b_1'].roi.x_offset == 1

                bodytf = hri.bodies['b_1'].transform()
                assert bodytf.transform.translation.x == 1.0
                assert bodytf.transform.translation.y == 1.0
                assert bodytf.transform.translation.z == 1.0

            hri.close()

        finally:
            py_spring_hri.defaults.body_topic_namespace = tmp_body_namespace
            py_spring_hri.defaults.body_tf_template = tmp_tf_body

            my_bodies_tracked_publisher.unregister()
            my_body_property_publisher.unregister()


    def test_transform(self):

        hri = HRIListener(subscribe_persons=True, subscribe_bodies=True, subscribe_faces=True, subscribe_voices=True, subscribe_groups=True,
                          default_target_frame='map')
        hri.start()

        # bodies
        self.ros_interface.publish_tracked_bodies(['b_1'])
        self.ros_interface.publish_tf('body_b_1')
        rospy.sleep(0.5)

        with hri.bodies.lock:
            tf = hri.bodies['b_1'].transform()

            assert tf.transform.translation.x == 0.0
            assert tf.transform.translation.y == 0.0
            assert tf.transform.translation.z == 0.0

        # faces
        self.ros_interface.publish_tracked_faces(['f_1'])
        self.ros_interface.publish_tf('face_f_1')
        self.ros_interface.publish_tf('gaze_f_1', pos=[1.0, 1.0, 1.0, 0.0])
        self.ros_interface.publish_tf('focus_f_1', pos=[2.0, 2.0, 2.0, 0.0])
        rospy.sleep(0.5)

        with hri.faces.lock:
            tf = hri.faces['f_1'].transform()

            assert tf.transform.translation.x == 0.0
            assert tf.transform.translation.y == 0.0
            assert tf.transform.translation.z == 0.0

            tf = hri.faces['f_1'].transform_gaze()

            assert tf.transform.translation.x == 1.0
            assert tf.transform.translation.y == 1.0
            assert tf.transform.translation.z == 1.0

            tf = hri.faces['f_1'].transform_focus()

            assert tf.transform.translation.x == 2.0
            assert tf.transform.translation.y == 2.0
            assert tf.transform.translation.z == 2.0

        # voices
        self.ros_interface.publish_tracked_voices(['v_1'])
        self.ros_interface.publish_tf('voice_v_1', pos=[3.0, 3.0, 3.0, 0.0])
        rospy.sleep(0.5)

        with hri.voices.lock:
            tf = hri.voices['v_1'].transform()

            assert tf.transform.translation.x == 3.0
            assert tf.transform.translation.y == 3.0
            assert tf.transform.translation.z == 3.0

        # persons
        self.ros_interface.publish_tracked_persons(['p_1'])
        self.ros_interface.publish_tf('person_p_1', pos=[4.0, 4.0, 4.0, 0.0])
        rospy.sleep(0.5)

        with hri.tracked_persons.lock:
            tf = hri.tracked_persons['p_1'].transform()

            assert tf.transform.translation.x == 4.0
            assert tf.transform.translation.y == 4.0
            assert tf.transform.translation.z == 4.0

        # groups
        self.ros_interface.publish_tracked_groups(['g_1'])
        self.ros_interface.publish_tf('group_g_1', pos=[5.0, 5.0, 5.0, 0.0])
        rospy.sleep(0.5)

        with hri.groups.lock:
            tf = hri.groups['g_1'].transform()

            assert tf.transform.translation.x == 5.0
            assert tf.transform.translation.y == 5.0
            assert tf.transform.translation.z == 5.0

        hri.close()


    def test_imgmsg_to_cv2_property_data_function(self):

        hri = HRIListener()
        hri.add_face_property('cropped')
        hri.start()

        ####################
        # 1 face
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_face(face_id='face_1', cropped=(128, 64))
            self.ros_interface.publish_tracked_faces(['face_1'])
            pub_rate.sleep()

        with hri.faces.lock:
            cropped = hri.faces['face_1'].cropped

            assert cropped.header is not None
            assert cropped.height == 128
            assert cropped.width == 64
            assert cropped.image.shape[0] == 128
            assert cropped.image.shape[1] == 64

        hri.close()


    def test_faces(self):

        hri = HRIListener.start_with_default_properties()

        ####################
        # 1 face
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_face(face_id='face_1', roi=(1, 2, 3, 4), cropped=(128, 128), aligned=(64, 64), frontalized=(32, 32),
                                            facs=None, expression='neutral', landmarks=[], softbiometrics=(30, 0))
            self.ros_interface.publish_tracked_faces(['face_1'])
            pub_rate.sleep()

        with hri.faces.lock:
            assert len(hri.faces) == 1
            assert 'face_1' in hri.faces

            face_1 = hri.faces['face_1']

            assert face_1.id == 'face_1'
            assert face_1.roi.x_offset == 1
            assert face_1.roi.y_offset == 2
            assert face_1.roi.height == 3
            assert face_1.roi.width == 4

            assert face_1.cropped.height == 128
            assert face_1.cropped.width == 128
            assert face_1.aligned.height == 64
            assert face_1.aligned.width == 64
            assert face_1.frontalized.height == 32
            assert face_1.frontalized.width == 32
            assert len(face_1.landmarks.landmarks) == 0
            assert len(face_1.facs.FAU) == 0
            assert face_1.expression.expression == 'neutral'
            assert face_1.softbiometrics.age == 30
            assert face_1.softbiometrics.gender == 0

        ################
        # 2 faces
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_face(face_id='face_1', roi=(1, 2, 3, 4), cropped=(128, 128), aligned=(64, 64), frontalized=(32, 32),
                                            facs=None, expression='neutral', landmarks=[], softbiometrics=(30, 0))
            self.ros_interface.publish_face(face_id='face_2', roi=(10, 20, 30, 40), cropped=(12, 12), aligned=(6, 6), frontalized=(3, 3),
                                            facs=None, expression='angry', landmarks=[], softbiometrics=(10, 1))
            self.ros_interface.publish_tracked_faces(['face_1', 'face_2'])
            pub_rate.sleep()

        with hri.faces.lock:
            assert len(hri.faces) == 2
            assert 'face_1' in hri.faces
            assert 'face_2' in hri.faces

            assert face_1.id == 'face_1'
            assert face_1.roi.x_offset == 1
            assert face_1.roi.y_offset == 2
            assert face_1.roi.height == 3
            assert face_1.roi.width == 4
            assert face_1.cropped.height == 128
            assert face_1.cropped.width == 128
            assert face_1.aligned.height == 64
            assert face_1.aligned.width == 64
            assert face_1.frontalized.height == 32
            assert face_1.frontalized.width == 32
            assert len(face_1.landmarks.landmarks) == 0
            assert len(face_1.facs.FAU) == 0
            assert face_1.expression.expression == 'neutral'
            assert face_1.softbiometrics.age == 30
            assert face_1.softbiometrics.gender == 0

            face_2 = hri.faces['face_2']
            assert face_2.id == 'face_2'
            assert face_2.roi.x_offset == 10
            assert face_2.roi.y_offset == 20
            assert face_2.roi.height == 30
            assert face_2.roi.width == 40
            assert face_2.cropped.height == 12
            assert face_2.cropped.width == 12
            assert face_2.aligned.height == 6
            assert face_2.aligned.width == 6
            assert face_2.frontalized.height == 3
            assert face_2.frontalized.width == 3
            assert len(face_2.landmarks.landmarks) == 0
            assert len(face_2.facs.FAU) == 0
            assert face_2.expression.expression == 'angry'
            assert face_2.softbiometrics.age == 10
            assert face_2.softbiometrics.gender == 1

        ################
        # 1 faces
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_face(face_id='face_2', roi=(10, 20, 30, 40), cropped=(12, 12), aligned=(6, 6), frontalized=(3, 3),
                                            facs=None, expression='surprised', landmarks=[], softbiometrics=(10, 1))
            self.ros_interface.publish_tracked_faces(['face_2'])
            pub_rate.sleep()

        with hri.faces.lock:
            assert len(hri.faces) == 1
            assert 'face_2' in hri.faces

            face_2 = hri.faces['face_2']
            assert face_2.id == 'face_2'
            assert face_2.roi.x_offset == 10
            assert face_2.roi.y_offset == 20
            assert face_2.roi.height == 30
            assert face_2.roi.width == 40
            assert face_2.cropped.height == 12
            assert face_2.cropped.width == 12
            assert face_2.aligned.height == 6
            assert face_2.aligned.width == 6
            assert face_2.frontalized.height == 3
            assert face_2.frontalized.width == 3
            assert len(face_2.landmarks.landmarks) == 0
            assert len(face_2.facs.FAU) == 0
            assert face_2.expression.expression == 'surprised'
            assert face_2.softbiometrics.age == 10
            assert face_2.softbiometrics.gender == 1

        ################
        # 0 faces
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_tracked_faces([])
            pub_rate.sleep()

        with hri.faces.lock:
            assert len(hri.faces) == 0

        hri.close()


    def my_tracked_entity_list_callback(self, msg):
        self._tracked_entities_callback_counter += 1
        self._tracked_entities_callback_args = msg


    def test_tracked_callbacks(self):

        hri = HRIListener.with_default_properties()
        hri.add_tracked_persons_callback(self.my_tracked_entity_list_callback)
        hri.add_known_persons_callback(self.my_tracked_entity_list_callback)
        hri.add_tracked_bodies_callback(self.my_tracked_entity_list_callback)
        hri.add_tracked_faces_callback(self.my_tracked_entity_list_callback)
        hri.add_tracked_voices_callback(self.my_tracked_entity_list_callback)
        hri.add_tracked_groups_callback(self.my_tracked_entity_list_callback)
        hri.start()

        #############
        # tracked persons
        self._tracked_entities_callback_counter = 0

        self.ros_interface.publish_tracked_persons(['person_1'])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 1
        assert len(self._tracked_entities_callback_args.ids) == 1
        assert 'person_1' in self._tracked_entities_callback_args.ids

        self.ros_interface.publish_tracked_persons([])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 2
        assert len(self._tracked_entities_callback_args.ids) == 0

        #############
        # known persons
        self._tracked_entities_callback_counter = 0

        self.ros_interface.publish_known_persons(['person_1'])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 1
        assert len(self._tracked_entities_callback_args.ids) == 1
        assert 'person_1' in self._tracked_entities_callback_args.ids

        self.ros_interface.publish_known_persons([])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 2
        assert len(self._tracked_entities_callback_args.ids) == 0

        #############
        # tracked bodies
        self._tracked_entities_callback_counter = 0

        self.ros_interface.publish_tracked_bodies(['body_1'])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 1
        assert len(self._tracked_entities_callback_args.ids) == 1
        assert 'body_1' in self._tracked_entities_callback_args.ids

        self.ros_interface.publish_tracked_bodies([])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 2
        assert len(self._tracked_entities_callback_args.ids) == 0

        #############
        # tracked faces
        self._tracked_entities_callback_counter = 0

        self.ros_interface.publish_tracked_faces(['face_1'])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 1
        assert len(self._tracked_entities_callback_args.ids) == 1
        assert 'face_1' in self._tracked_entities_callback_args.ids

        self.ros_interface.publish_tracked_faces([])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 2
        assert len(self._tracked_entities_callback_args.ids) == 0

        #############
        # tracked voices
        self._tracked_entities_callback_counter = 0

        self.ros_interface.publish_tracked_voices(['voice_1'])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 1
        assert len(self._tracked_entities_callback_args.ids) == 1
        assert 'voice_1' in self._tracked_entities_callback_args.ids

        self.ros_interface.publish_tracked_voices([])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 2
        assert len(self._tracked_entities_callback_args.ids) == 0

        #############
        # tracked groups
        self._tracked_entities_callback_counter = 0

        self.ros_interface.publish_tracked_groups(['group_1'])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 1
        assert len(self._tracked_entities_callback_args.ids) == 1
        assert 'group_1' in self._tracked_entities_callback_args.ids

        self.ros_interface.publish_tracked_groups([])
        rospy.sleep(1)
        assert self._tracked_entities_callback_counter == 2
        assert len(self._tracked_entities_callback_args.ids) == 0

        hri.close()


    def my_tracked_entity_property_list_callback(self, entity, data, msg):
        self._tracked_entity_property_callback_counter += 1
        self._tracked_entity_property_callback_args = (entity, data, msg)

    def test_entity_property_callbacks(self):

        self._tracked_entity_property_callback_counter = 0

        self.ros_interface.publish_tracked_persons([])
        rospy.sleep(1)

        hri = HRIListener.with_default_properties()
        hri.add_person_property_callback('alias', self.my_tracked_entity_property_list_callback)
        hri.start()

        # create person
        self.ros_interface.publish_tracked_persons(['person_1'])
        rospy.sleep(1)

        # update the persons properties
        self.ros_interface.publish_person(person_id='person_1', alias='test_alias')
        rospy.sleep(1)

        assert self._tracked_entity_property_callback_counter == 1

        assert self._tracked_entity_property_callback_args[0] == hri.tracked_persons['person_1']
        assert self._tracked_entity_property_callback_args[1] == 'test_alias'
        assert isinstance(self._tracked_entity_property_callback_args[2], String)
        assert self._tracked_entity_property_callback_args[2].data == 'test_alias'

        hri.close()


    def test_voices(self):

        hri = HRIListener.start_with_default_properties()

        ####################
        # 1 voice
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_voice(voice_id='voice_1', audio=[1,2,3], features=0.0, is_speaking=False, speech='hallo')
            self.ros_interface.publish_tracked_voices(['voice_1'])
            pub_rate.sleep()

        with hri.voices.lock:
            assert len(hri.voices) == 1
            assert 'voice_1' in hri.voices

            voice_1 = hri.voices['voice_1']

            assert voice_1.id == 'voice_1'
            assert len(voice_1.audio) == 3
            assert voice_1.features.pitch == 0.0
            assert voice_1.is_speaking == False
            assert voice_1.speech.final == 'hallo'

        ################
        # 2 voices
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_voice(voice_id='voice_1', audio=[1,2,3,4], features=1.0, is_speaking=False, speech='hallo1')
            self.ros_interface.publish_voice(voice_id='voice_2', audio=[10,20,30,40, 50], features=2.0, is_speaking=True, speech='hallo2')
            self.ros_interface.publish_tracked_voices(['voice_1', 'voice_2'])
            pub_rate.sleep()

        with hri.voices.lock:
            assert len(hri.voices) == 2
            assert 'voice_1' in hri.voices
            assert 'voice_2' in hri.voices

            voice_1 = hri.voices['voice_1']
            assert voice_1.id == 'voice_1'
            assert len(voice_1.audio) == 4
            assert voice_1.features.pitch == 1.0
            assert voice_1.is_speaking == False
            assert voice_1.speech.final == 'hallo1'

            voice_2 = hri.voices['voice_2']
            assert voice_2.id == 'voice_2'
            assert len(voice_2.audio) == 5
            assert voice_2.features.pitch == 2.0
            assert voice_2.is_speaking == True
            assert voice_2.speech.final == 'hallo2'

        ################
        # 1 voices
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_voice(voice_id='voice_2', audio=[10,20,30,40,50,60], features=3.0, is_speaking=False, speech='hallo3')
            self.ros_interface.publish_tracked_voices(['voice_2'])
            pub_rate.sleep()

        with hri.voices.lock:
            assert len(hri.voices) == 1
            assert 'voice_2' in hri.voices

            voice_2 = hri.voices['voice_2']
            assert voice_2.id == 'voice_2'
            assert len(voice_2.audio) == 6
            assert voice_2.features.pitch == 3.0
            assert voice_2.is_speaking == False
            assert voice_2.speech.final == 'hallo3'

        ################
        # 0 voices
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_tracked_voices([])
            pub_rate.sleep()

        with hri.voices.lock:
            assert len(hri.voices) == 0

        hri.close()


    def test_bodies(self):

        hri = HRIListener()
        hri.add_body_property('roi')
        hri.add_body_property('cropped')
        hri.start()

        ####################
        # publish a body
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_body(body_id='body_1')
            self.ros_interface.publish_tracked_bodies(['body_1'])
            pub_rate.sleep()

        with hri.bodies.lock:
            assert len(hri.bodies) == 1
            assert 'body_1' in hri.bodies

            assert hri.bodies['body_1'].id == 'body_1'
            assert hri.bodies['body_1'].roi.x_offset == 1
            assert hri.bodies['body_1'].roi.y_offset == 2
            assert hri.bodies['body_1'].roi.height == 3
            assert hri.bodies['body_1'].roi.width == 4

            assert hri.bodies['body_1'].cropped.height == 512

        ####################
        # publish two bodies
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_body(body_id='body_1')
            self.ros_interface.publish_body(body_id='body_2')
            self.ros_interface.publish_tracked_bodies(['body_1', 'body_2'])
            pub_rate.sleep()

        with hri.bodies.lock:
            assert len(hri.bodies) == 2
            assert 'body_1' in hri.bodies
            assert 'body_2' in hri.bodies

            assert hri.bodies['body_1'].id == 'body_1'
            assert hri.bodies['body_1'].roi.x_offset == 1
            assert hri.bodies['body_1'].roi.y_offset == 2
            assert hri.bodies['body_1'].roi.height == 3
            assert hri.bodies['body_1'].roi.width == 4

            assert hri.bodies['body_1'].cropped.height == 512

            assert hri.bodies['body_2'].id == 'body_2'
            assert hri.bodies['body_2'].roi.x_offset == 1
            assert hri.bodies['body_2'].roi.y_offset == 2
            assert hri.bodies['body_2'].roi.height == 3
            assert hri.bodies['body_2'].roi.width == 4

            assert hri.bodies['body_1'].cropped.height == 512

        ####################
        # publish a body
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_body(body_id='body_1')
            self.ros_interface.publish_tracked_bodies(['body_1'])
            pub_rate.sleep()

        with hri.bodies.lock:
            assert len(hri.bodies) == 1
            assert 'body_1' in hri.bodies

            assert hri.bodies['body_1'].id == 'body_1'
            assert hri.bodies['body_1'].roi.x_offset == 1
            assert hri.bodies['body_1'].roi.y_offset == 2
            assert hri.bodies['body_1'].roi.height == 3
            assert hri.bodies['body_1'].roi.width == 4

            assert hri.bodies['body_1'].cropped.height == 512

        ####################
        # publish no body
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_tracked_bodies([])
            pub_rate.sleep()

        with hri.bodies.lock:
            assert len(hri.bodies) == 0

        hri.close()


    def test_persons(self):
        # TODO: add voices and faces
        hri = HRIListener.start_with_default_properties()

        ####################
        # publish a person and body
        pub_rate = rospy.Rate(5)
        for _ in range(5):
            self.ros_interface.publish_body(body_id='body_1')
            self.ros_interface.publish_tracked_bodies(['body_1'])
            self.ros_interface.publish_person(person_id='person_1', body_id='body_1')
            self.ros_interface.publish_tracked_persons(['person_1'])
            self.ros_interface.publish_known_persons(['person_1'])
            pub_rate.sleep()

        rospy.sleep(2)

        with hri.tracked_persons.lock:
            assert len(hri.tracked_persons) == 1
            assert 'person_1' in hri.tracked_persons

            assert hri.tracked_persons['person_1'].id == 'person_1'
            assert hri.tracked_persons['person_1'].alias == 'something'
            assert hri.tracked_persons['person_1'].engagement_status.level == 1

            assert hri.tracked_persons['person_1'].body_id == 'body_1'
            assert hri.tracked_persons['person_1'].body == hri.bodies['body_1']

            with hri.known_persons.lock:
                assert len(hri.known_persons) == 1
                assert 'person_1' in hri.known_persons
                assert hri.known_persons['person_1'] == hri.tracked_persons['person_1']

        ####################
        # publish two persons
        pub_rate = rospy.Rate(5)
        for _ in range(5):
            self.ros_interface.publish_body(body_id='body_1')
            self.ros_interface.publish_body(body_id='body_2')
            self.ros_interface.publish_tracked_bodies(['body_1', 'body_2'])
            self.ros_interface.publish_person(person_id='person_1', body_id='body_1')
            self.ros_interface.publish_person(person_id='person_2', body_id='body_2')
            self.ros_interface.publish_tracked_persons(['person_1', 'person_2'])
            self.ros_interface.publish_known_persons(['person_1', 'person_2'])
            pub_rate.sleep()

        rospy.sleep(2)

        with hri.tracked_persons.lock:
            assert len(hri.tracked_persons) == 2
            assert 'person_1' in hri.tracked_persons
            assert 'person_2' in hri.tracked_persons

            assert hri.tracked_persons['person_1'].id == 'person_1'
            assert hri.tracked_persons['person_1'].alias == 'something'
            assert hri.tracked_persons['person_1'].engagement_status.level == 1

            assert hri.tracked_persons['person_1'].body_id == 'body_1'
            assert hri.tracked_persons['person_1'].body == hri.bodies['body_1']

            assert hri.tracked_persons['person_2'].id == 'person_2'
            assert hri.tracked_persons['person_2'].alias == 'something'
            assert hri.tracked_persons['person_2'].engagement_status.level == 1

            assert hri.tracked_persons['person_2'].body_id == 'body_2'
            assert hri.tracked_persons['person_2'].body == hri.bodies['body_2']

            with hri.known_persons.lock:
                assert len(hri.known_persons) == 2
                assert 'person_1' in hri.known_persons
                assert 'person_2' in hri.known_persons
                assert hri.known_persons['person_1'] == hri.tracked_persons['person_1']
                assert hri.known_persons['person_2'] == hri.tracked_persons['person_2']

        ####################
        # publish a person
        pub_rate = rospy.Rate(5)
        for _ in range(5):
            self.ros_interface.publish_body(body_id='body_1')
            self.ros_interface.publish_tracked_bodies(['body_1'])
            self.ros_interface.publish_person(person_id='person_1', body_id='body_1')
            self.ros_interface.publish_tracked_persons(['person_1'])
            self.ros_interface.publish_known_persons(['person_1', 'person_2'])
            pub_rate.sleep()

        rospy.sleep(2)

        with hri.tracked_persons.lock:
            assert len(hri.tracked_persons) == 1
            assert 'person_1' in hri.tracked_persons

            assert hri.tracked_persons['person_1'].id == 'person_1'
            assert hri.tracked_persons['person_1'].alias == 'something'
            assert hri.tracked_persons['person_1'].engagement_status.level == 1

            assert hri.tracked_persons['person_1'].body_id == 'body_1'
            assert hri.tracked_persons['person_1'].body == hri.bodies['body_1']

            with hri.known_persons.lock:
                assert len(hri.known_persons) == 2
                assert 'person_1' in hri.known_persons
                assert 'person_2' in hri.known_persons
                assert hri.known_persons['person_1'] == hri.tracked_persons['person_1']

        ####################
        # publish no persons
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_tracked_persons([])
            self.ros_interface.publish_known_persons([])
            pub_rate.sleep()

        with hri.tracked_persons.lock:
            assert len(hri.known_persons) == 0

        with hri.known_persons.lock:
            assert len(hri.tracked_persons) == 0

        hri.close()


    def test_custom_properties(self):

        hri = HRIListener.with_default_properties()
        hri.add_person_property('custom_property', String, lambda msg: eval(msg.data))
        hri.start()

        ####################
        pub_rate = rospy.Rate(5)
        for _ in range(5):
            self.ros_interface.publish_person(person_id='person_1')
            self.ros_interface.publish_tracked_persons(['person_1'])
            pub_rate.sleep()

        rospy.sleep(2)

        with hri.tracked_persons.lock:
            assert isinstance(hri.tracked_persons['person_1'].custom_property, list)
            assert len(hri.tracked_persons['person_1'].custom_property) == 4
            assert hri.tracked_persons['person_1'].custom_property[0] == 1
            assert hri.tracked_persons['person_1'].custom_property[1] == 2
            assert hri.tracked_persons['person_1'].custom_property[2] == 3
            assert hri.tracked_persons['person_1'].custom_property[3] == 4

        hri.close()


    def test_copy_methods(self):

        hri = HRIListener.with_default_properties()
        hri.add_person_property('custom_property', String, lambda msg: eval(msg.data))
        hri.start()

        ####################
        pub_rate = rospy.Rate(5)
        for _ in range(5):
            self.ros_interface.publish_body(body_id='body_1')
            self.ros_interface.publish_tracked_bodies(['body_1'])
            self.ros_interface.publish_person(person_id='person_1', body_id='body_1')
            self.ros_interface.publish_tracked_persons(['person_1'])
            self.ros_interface.publish_known_persons(['person_1'])
            pub_rate.sleep()
        rospy.sleep(2)

        cp_tracked_persons = hri.copy_tracked_persons()
        cp_known_persons = hri.copy_known_persons()
        cp_bodies = hri.copy_bodies()

        cp_data_tracked_persons = hri.copy_tracked_persons_data()
        cp_data_known_persons = hri.copy_known_persons_data()
        cp_data_bodies = hri.copy_bodies_data()

        deep_cp_data_tracked_persons = hri.deepcopy_tracked_persons_data()
        deep_cp_data_known_persons = hri.deepcopy_known_persons_data()
        deep_cp_data_bodies = hri.deepcopy_bodies_data()

        # check copies
        with hri.tracked_persons.lock:
            assert len(cp_tracked_persons) == 1
            assert 'person_1' in cp_tracked_persons
            assert cp_tracked_persons['person_1'] == hri.tracked_persons['person_1']

        with hri.known_persons.lock:
            assert len(cp_known_persons) == 1
            assert 'person_1' in cp_known_persons
            assert cp_known_persons['person_1'] == hri.known_persons['person_1']

        with hri.bodies.lock:
            assert len(cp_bodies) == 1
            assert 'body_1' in cp_bodies
            assert cp_bodies['body_1'] == hri.bodies['body_1']

        # check data copies
        assert len(cp_data_tracked_persons) == 1
        assert 'person_1' in cp_data_tracked_persons
        assert cp_data_tracked_persons['person_1'] != hri.tracked_persons['person_1']
        assert cp_data_tracked_persons['person_1'].body != hri.tracked_persons['person_1'].body
        assert cp_data_tracked_persons['person_1'].body.id == hri.tracked_persons['person_1'].body.id
        with hri._persons_lock:
            # custom_property is a list, thus copy must still have correct reference
            assert cp_data_tracked_persons['person_1'].custom_property == hri.tracked_persons['person_1'].custom_property

        assert len(cp_data_known_persons) == 1
        assert 'person_1' in cp_data_known_persons
        assert cp_data_known_persons['person_1'] != hri.known_persons['person_1']
        assert cp_data_known_persons['person_1'].body != hri.known_persons['person_1'].body
        assert cp_data_known_persons['person_1'].body.id == hri.known_persons['person_1'].body.id
        with hri._persons_lock:
            # custom_property is a list, thus copy must still have correct reference
            assert cp_data_tracked_persons['person_1'].custom_property is hri.known_persons['person_1'].custom_property

        assert len(cp_data_bodies) == 1
        assert 'body_1' in cp_data_bodies
        assert cp_data_bodies['body_1'] != hri.bodies['body_1']
        assert cp_data_bodies['body_1'].id == hri.bodies['body_1'].id

        # check deep copies
        assert len(deep_cp_data_tracked_persons) == 1
        assert 'person_1' in deep_cp_data_tracked_persons
        assert deep_cp_data_tracked_persons['person_1'] != hri.tracked_persons['person_1']
        assert deep_cp_data_tracked_persons['person_1'].body != hri.tracked_persons['person_1'].body
        assert deep_cp_data_tracked_persons['person_1'].body.id == hri.tracked_persons['person_1'].body.id
        with hri._persons_lock:
            # custom_property is a list, thus deepcopy must have same content but be a different list
            assert deep_cp_data_tracked_persons['person_1'].custom_property == hri.tracked_persons['person_1'].custom_property
            assert deep_cp_data_tracked_persons['person_1'].custom_property is not hri.tracked_persons['person_1'].custom_property

        assert len(deep_cp_data_known_persons) == 1
        assert 'person_1' in deep_cp_data_known_persons
        assert deep_cp_data_known_persons['person_1'] != hri.known_persons['person_1']
        assert deep_cp_data_known_persons['person_1'].body != hri.known_persons['person_1'].body
        assert deep_cp_data_known_persons['person_1'].body.id == hri.known_persons['person_1'].body.id
        with hri._persons_lock:
            # custom_property is a list, thus copy must still have correct reference
            assert deep_cp_data_known_persons['person_1'].custom_property == hri.known_persons['person_1'].custom_property
            assert deep_cp_data_known_persons['person_1'].custom_property is not hri.known_persons['person_1'].custom_property

        assert len(deep_cp_data_bodies) == 1
        assert 'body_1' in deep_cp_data_bodies
        assert deep_cp_data_bodies['body_1'] != hri.bodies['body_1']
        assert deep_cp_data_bodies['body_1'].id == hri.bodies['body_1'].id

        ####################
        # publish new data, thus the copies must have changed or not
        pub_rate = rospy.Rate(5)
        for _ in range(5):
            self.ros_interface.publish_body(body_id='body_1')
            self.ros_interface.publish_tracked_bodies(['body_1'])
            self.ros_interface.publish_person(person_id='person_1', body_id='body_1', alias='something else', engagement_status=2, custom_topic='[10,20]')
            self.ros_interface.publish_tracked_persons(['person_1'])
            self.ros_interface.publish_known_persons(['person_1'])
            pub_rate.sleep()
        rospy.sleep(2)

        # check copies
        with hri.tracked_persons.lock:
            assert len(cp_tracked_persons) == 1
            assert 'person_1' in cp_tracked_persons
            assert cp_tracked_persons['person_1'] == hri.tracked_persons['person_1']
            assert cp_tracked_persons['person_1'].alias == 'something else'
            assert cp_tracked_persons['person_1'].custom_property == [10, 20]

        with hri.known_persons.lock:
            assert len(cp_known_persons) == 1
            assert 'person_1' in cp_known_persons
            assert cp_known_persons['person_1'] == hri.known_persons['person_1']
            assert cp_known_persons['person_1'].custom_property == [10, 20]

        with hri.bodies.lock:
            assert len(cp_bodies) == 1
            assert 'body_1' in cp_bodies
            assert cp_bodies['body_1'] == hri.bodies['body_1']

        # check data copies
        assert len(cp_data_tracked_persons) == 1
        assert 'person_1' in cp_data_tracked_persons
        assert cp_data_tracked_persons['person_1'] != hri.tracked_persons['person_1']
        assert cp_data_tracked_persons['person_1'].body != hri.tracked_persons['person_1'].body
        assert cp_data_tracked_persons['person_1'].body.id == hri.tracked_persons['person_1'].body.id
        assert cp_data_tracked_persons['person_1'].alias == 'something'
        assert cp_data_tracked_persons['person_1'].custom_property == [1, 2, 3, 4]

        assert len(cp_data_known_persons) == 1
        assert 'person_1' in cp_data_known_persons
        assert cp_data_known_persons['person_1'] != hri.tracked_persons['person_1']
        assert cp_data_known_persons['person_1'].body != hri.tracked_persons['person_1'].body
        assert cp_data_known_persons['person_1'].body.id == hri.tracked_persons['person_1'].body.id
        assert cp_data_known_persons['person_1'].alias == 'something'
        assert cp_data_known_persons['person_1'].custom_property == [1, 2, 3, 4]

        assert len(cp_data_bodies) == 1
        assert 'body_1' in cp_data_bodies
        assert cp_data_bodies['body_1'] != hri.bodies['body_1']
        assert cp_data_bodies['body_1'].id == hri.bodies['body_1'].id

        # check deep copies
        assert len(deep_cp_data_tracked_persons) == 1
        assert 'person_1' in deep_cp_data_tracked_persons
        assert deep_cp_data_tracked_persons['person_1'] != hri.tracked_persons['person_1']
        assert deep_cp_data_tracked_persons['person_1'].body != hri.tracked_persons['person_1'].body
        assert deep_cp_data_tracked_persons['person_1'].body.id == hri.tracked_persons['person_1'].body.id
        with hri._persons_lock:
            # custom_property is a list, thus deepcopy must have same content but be a different list
            assert deep_cp_data_tracked_persons['person_1'].custom_property == [1, 2, 3, 4]
            assert deep_cp_data_tracked_persons['person_1'].custom_property is not hri.tracked_persons['person_1'].custom_property

        assert len(deep_cp_data_known_persons) == 1
        assert 'person_1' in deep_cp_data_known_persons
        assert deep_cp_data_known_persons['person_1'] != hri.known_persons['person_1']
        assert deep_cp_data_known_persons['person_1'].body != hri.known_persons['person_1'].body
        assert deep_cp_data_known_persons['person_1'].body.id == hri.known_persons['person_1'].body.id
        with hri._persons_lock:
            # custom_property is a list, thus copy must still have correct reference
            assert deep_cp_data_known_persons['person_1'].custom_property == [1, 2, 3, 4]
            assert deep_cp_data_known_persons['person_1'].custom_property is not hri.known_persons['person_1'].custom_property

        assert len(deep_cp_data_bodies) == 1
        assert 'body_1' in deep_cp_data_bodies
        assert deep_cp_data_bodies['body_1'] != hri.bodies['body_1']
        assert deep_cp_data_bodies['body_1'].id == hri.bodies['body_1'].id

        hri.close()


    def test_groups(self):

        hri = HRIListener(subscribe_groups=True, subscribe_persons=True)
        hri.start()

        ####################
        # publish a group and its members
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_tracked_persons(['person_1.1', 'person_1.2', 'person_1.3'])
            self.ros_interface.publish_group(group_id='group_1')
            self.ros_interface.publish_tracked_groups(['group_1'])
            pub_rate.sleep()

        with hri.groups.lock:
            assert len(hri.groups) == 1
            assert 'group_1' in hri.groups

            assert len(hri.groups['group_1'].member_ids) == 3
            assert 'person_1.1' in hri.groups['group_1'].member_ids
            assert 'person_1.2' in hri.groups['group_1'].member_ids
            assert 'person_1.3' in hri.groups['group_1'].member_ids


            assert len(hri.groups['group_1'].members) == 3
            assert 'person_1.1' in hri.groups['group_1'].members
            assert 'person_1.2' in hri.groups['group_1'].members
            assert 'person_1.3' in hri.groups['group_1'].members

        ####################
        # publish two groups
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_tracked_persons([])
            self.ros_interface.publish_known_persons(['person_1.1', 'person_1.2', 'person_1.3', 'person_2.1', 'person_2.2', 'person_2.3'])
            self.ros_interface.publish_group(group_id='group_1')
            self.ros_interface.publish_group(group_id='group_2')
            self.ros_interface.publish_tracked_groups(['group_1', 'group_2'])
            pub_rate.sleep()

        with hri.groups.lock:
            assert len(hri.groups) == 2
            assert 'group_1' in hri.groups
            assert 'group_2' in hri.groups

            assert len(hri.groups['group_1'].members) == 3
            assert 'person_1.1' in hri.groups['group_1'].members
            assert 'person_1.2' in hri.groups['group_1'].members
            assert 'person_1.3' in hri.groups['group_1'].members

            assert len(hri.groups['group_1'].member_ids) == 3
            assert 'person_1.1' in hri.groups['group_1'].member_ids
            assert 'person_1.2' in hri.groups['group_1'].member_ids
            assert 'person_1.3' in hri.groups['group_1'].member_ids

            assert len(hri.groups['group_2'].members) == 3
            assert 'person_2.1' in hri.groups['group_2'].members
            assert 'person_2.2' in hri.groups['group_2'].members
            assert 'person_2.3' in hri.groups['group_2'].members

            assert len(hri.groups['group_2'].member_ids) == 3
            assert 'person_2.1' in hri.groups['group_2'].member_ids
            assert 'person_2.2' in hri.groups['group_2'].member_ids
            assert 'person_2.3' in hri.groups['group_2'].member_ids

        ####################
        # remove one group and publish one groups
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_tracked_persons(['person_2.1', 'person_2.2', 'person_2.3'])
            self.ros_interface.publish_group(group_id='group_2')
            self.ros_interface.publish_tracked_groups(['group_2'])
            pub_rate.sleep()

        with hri.groups.lock:
            assert len(hri.groups) == 1
            assert 'group_2' in hri.groups

            assert len(hri.groups['group_2'].members) == 3
            assert 'person_2.1' in hri.groups['group_2'].members
            assert 'person_2.2' in hri.groups['group_2'].members
            assert 'person_2.3' in hri.groups['group_2'].members

            assert len(hri.groups['group_2'].member_ids) == 3
            assert 'person_2.1' in hri.groups['group_2'].member_ids
            assert 'person_2.2' in hri.groups['group_2'].member_ids
            assert 'person_2.3' in hri.groups['group_2'].member_ids

        ####################
        # publish no group
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_tracked_groups([])
            pub_rate.sleep()

        with hri.groups.lock:
            assert len(hri.groups) == 0

        hri.close()


    def test_group_dict_lock_thread_safety(self):

        hri = HRIListener(subscribe_groups=True)
        hri.start()

        # create a thread that publishes continuously new groups
        group_creation_thread = threading.Thread(target=self.my_group_creation_thread)
        group_creation_thread.start()

        # check if the groups can be accessed while abother thread is writing groups
        while group_creation_thread.is_alive():
            with hri.groups.lock:
                for group_id, group in hri.groups.items():
                    assert group_id in ['group_0', 'group_1', 'group_2', 'group_3', 'group_4', 'group_5', 'group_6', 'group_7', 'group_8', 'group_9']

                    group_idx = int(group_id[-1])

                    if group.member_ids is not None:
                        assert len(group.member_ids) == 3
                        assert 'person_{}.1'.format(group_idx) in group.member_ids
                        assert 'person_{}.2'.format(group_idx) in group.member_ids
                        assert 'person_{}.3'.format(group_idx) in group.member_ids

                    rospy.sleep(0.05)

        hri.close()


    def test_group_dict_copy_thread_safety(self):

        hri = HRIListener(subscribe_groups=True)
        hri.start()

        # create a thread that publishes continuously new groups
        group_creation_thread = threading.Thread(target=self.my_group_creation_thread)
        group_creation_thread.start()

        # check if the groups can be accessed after copying them while another thread is writing groups
        while group_creation_thread.is_alive():

            # copy the groups
            copied_groups = hri.copy_groups()

            for group_id, group in copied_groups.items():
                assert group_id in ['group_0', 'group_1', 'group_2', 'group_3', 'group_4', 'group_5', 'group_6', 'group_7', 'group_8', 'group_9']
                group_idx = int(group_id[-1])

                if group.member_ids is not None:
                    assert len(group.member_ids) == 3
                    assert 'person_{}.1'.format(group_idx) in group.member_ids
                    assert 'person_{}.2'.format(group_idx) in group.member_ids
                    assert 'person_{}.3'.format(group_idx) in group.member_ids

        hri.close()


    def test_tf_properties(self):

        hri = HRIListener()
        hri.add_group_property('tf', String, 'tf:map')
        hri.start()

        # publish a group that has a tf
        pub_rate = rospy.Rate(10)
        for _ in range(5):
            self.ros_interface.publish_group('group_1')
            self.ros_interface.publish_tf('group_1', [1.0, 1.0, 1.0, 0.0])
            self.ros_interface.publish_group_tf('group_1','group_1')
            self.ros_interface.publish_tracked_groups(['group_1'])
            pub_rate.sleep()

        assert len(hri.groups) == 1
        assert isinstance(hri.groups['group_1'].tf, TransformStamped)

        hri.close()


    def test_constructors(self):

        # start constructor

        hri = HRIListener.start(subscribe_persons=True, subscribe_bodies=True)

        ####################
        # publish a person and body
        pub_rate = rospy.Rate(5)
        for _ in range(5):
            self.ros_interface.publish_body(body_id='body_1')
            self.ros_interface.publish_tracked_bodies(['body_1'])
            self.ros_interface.publish_person(person_id='person_1', body_id='body_1')
            self.ros_interface.publish_tracked_persons(['person_1'])
            self.ros_interface.publish_known_persons(['person_1'])
            pub_rate.sleep()

        rospy.sleep(0.5)

        with hri.tracked_persons.lock:
            assert len(hri.tracked_persons) == 1
            assert 'person_1' in hri.tracked_persons

            assert hri.tracked_persons['person_1'].id == 'person_1'
            assert hri.tracked_persons['person_1'].body_id == 'body_1'
            assert hri.tracked_persons['person_1'].body == hri.bodies['body_1']

            with hri.known_persons.lock:
                assert len(hri.known_persons) == 1
                assert 'person_1' in hri.known_persons
                assert hri.known_persons['person_1'] == hri.tracked_persons['person_1']

        with hri.bodies.lock:
            assert len(hri.bodies) == 1
            assert 'body_1' in hri.bodies

            assert hri.bodies['body_1'].id == 'body_1'

        hri.close()



if __name__ == '__main__':
    rostest.rosrun('py_spring_hri', 'test_py_spring_hri', PySpringHriInterfaceCase, *sys.argv)